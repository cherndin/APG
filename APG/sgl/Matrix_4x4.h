﻿#pragma once


#ifndef MATRIX_4x4_H
#define MATRIX_4x4_H

#include "stdint.h"
#include "Vector_4.h"
#include <iostream>
#include <cmath>

using namespace std;


class Matrix_4x4 {
public:
	Matrix_4x4();
	~Matrix_4x4() {};


	void setOnPosition(int index, float number);

	Matrix_4x4& createViewportMatrix(int64_t x, int64_t y, uint64_t width, uint64_t height);

	Matrix_4x4& createIdentityMatrix();

	Matrix_4x4& createOrthoMatrix(float left, float right, float bottom, float top, float near, float far);

	Matrix_4x4 createRotate2DMatrix(float angle, float centerx, float centery);

	Matrix_4x4 createScaleMatrix(float scalex, float scaley, float scalez);

	Matrix_4x4 createRotateYMatrix(float angle);

	Matrix_4x4 createTranslateMatrix(float x, float y, float z);

	Matrix_4x4 createFrustrumMatrix(float left, float right, float bottom, float top, float near, float far);


	Matrix_4x4 frustrum(float left, float right, float bottom, float top, float near, float far);



	Matrix_4x4 multiply(Matrix_4x4 operand); 

	Matrix_4x4 multiply(float operand);


	Vector_4 multiplyByVector_4(Vector_4 o);

	//Vector_3 multiplyVector_3ByMatrix(Vector_3 v);

	//Vector_4 multiplyVector_4ByMatrix(Vector_4 v);

	Matrix_4x4 createMatrix(const float* m);


	Matrix_4x4 createTransposedMatrix(const float* m);

	int isRegular(int sizeOfMatrix);

	float** create2DimensionalMatrix(float* matrix);

	Matrix_4x4 testMatrix();

	Matrix_4x4 inverse();

	Matrix_4x4 adjoint();

	//float det3x3();


	void coutMatrix();

	float matrix[16];

	int32_t viewportWidth;
	int32_t viewportHeight;
};

#endif