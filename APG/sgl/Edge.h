#ifndef EDGE_H
#define EDGE_H

#include "Vertex.h"

using namespace std;

class Edge {
public:
	Edge(Vertex v1, Vertex v2);

	~Edge() {};

	void calculateIntersection(float y);

	void coutEdge();

	bool equals(Edge v);


	Vertex v1, v2;

	float x1, y1, x2, y2, 
		zBottom, zTop,
		intersection; // bottom and top z


};

#endif