#include "Ray.h"

Ray::Ray() {
	
}


Ray::Ray(Vector_3 origin, Vector_3 direction) : origin(origin), direction(direction) {
	depth = 0;
	tmin = 0.0;
	tmax = FLOAT_MAX_VALUE;
}


Ray::Ray(Vector_3 origin, Vector_3 direction, int depth) : origin(origin), direction(direction), depth(depth){
	tmin = 0.0;
	tmax = FLOAT_MAX_VALUE;
}

Ray::Ray(Vector_3 origin, Vector_3 direction, int depth, int tmin, int tmax) : origin(origin), direction(direction), depth(depth), tmin(tmin), tmax(tmax) {

}

Ray::Ray(Vector_3 origin, Vector_3 direction, int tmin, int tmax) : origin(origin), direction(direction), tmin(tmin), tmax(tmax) {
	depth = 0;
}

bool Ray::isRefracted(Ray ray, Vector_3 normal, float ior) {
	float gamma, sqrterm;
	float dot = ray.direction.dotProduct(normal);

	if (dot < 0.0) {
		// from outside into the inside of object
		gamma = 1.0 / ior;
	}
	else {
		// from the inside to outside of object
		gamma = ior;
		dot = -dot;
		normal = normal.multiplyByFloat(-1.0);
	}
	sqrterm = 1.0 - gamma * gamma * (1.0 - dot * dot);

	// Check for total internal reflection, do nothing if it applies.
	if (sqrterm > 0.0) {
		sqrterm = dot * gamma + sqrt(sqrterm);
		//direction of a refracted ray
		direction = ray.direction.multiplyByFloat(gamma).add(normal.multiplyByFloat(sqrterm*(-1.0))).normalize();
		return true;
	}

	return false;
}