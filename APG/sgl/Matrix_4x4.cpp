#include "Matrix_4x4.h"


Matrix_4x4::Matrix_4x4() {
	matrix[0] = 0.0f;
	matrix[1] = 0.0f;
	matrix[2] = 0.0f;
	matrix[3] = 0.0f;

	matrix[4] = 0.0f;
	matrix[5] = 0.0f;
	matrix[6] = 0.0f;
	matrix[7] = 0.0f;

	matrix[8] = 0.0f;
	matrix[9] = 0.0f;
	matrix[10] = 0.0f;
	matrix[11] = 0.0f;

	matrix[12] = 0.0f;
	matrix[13] = 0.0f;
	matrix[14] = 0.0f;
	matrix[15] = 0.0f;
	//auto array_length = end(matrix) - begin(matrix);

	//int size = sizeof(matrix) ;
}

void Matrix_4x4::setOnPosition(int index, float number) {
	matrix[index] = number;
}

Matrix_4x4 & Matrix_4x4::createViewportMatrix(int64_t x, int64_t y, uint64_t width, uint64_t height) {

	x = (float)x; // or static_cast?
	y = (float)y;
	viewportWidth = width = (float)width;
	viewportHeight = height = (float)height;

	matrix[0] = width / 2.0f;
	matrix[1] = 0.0f;
	matrix[2] = 0.0f;
	matrix[3] = x + width / 2.0f;

	matrix[4] = 0.0f;
	matrix[5] = height / 2.0f;
	matrix[6] = 0.0f;
	matrix[7] = y + height / 2.0f;

	matrix[8] = 0.0f;
	matrix[9] = 0.0f;
	matrix[10] = 0.5f;
	matrix[11] = 0.5f;

	matrix[12] = 0.0f;
	matrix[13] = 0.0f;
	matrix[14] = 0.0f;
	matrix[15] = 1.0f;

	return *this;
}


Matrix_4x4& Matrix_4x4::createIdentityMatrix() {

	matrix[0] = 1.0f;
	matrix[1] = 0.0f;
	matrix[2] = 0.0f;
	matrix[3] = 0.0f;

	matrix[4] = 0.0f;
	matrix[5] = 1.0f;
	matrix[6] = 0.0f;
	matrix[7] = 0.0f;

	matrix[8] = 0.0f;
	matrix[9] = 0.0f;
	matrix[10] = 1.0f;
	matrix[11] = 0.0f;

	matrix[12] = 0.0f;
	matrix[13] = 0.0f;
	matrix[14] = 0.0f;
	matrix[15] = 1.0f;

	return *this;
}

Matrix_4x4& Matrix_4x4::createOrthoMatrix(float left, float right, float bottom, float top, float near, float far) {
	//Matrix_4x4* result = new Matrix_4x4();
	matrix[0] = 2.0f / (right - left);
	matrix[1] = 0.0f;
	matrix[2] = 0.0f;
	matrix[3] = -(right + left) / (right - left);

	matrix[4] = 0.0f;
	matrix[5] = 2 / (top - bottom);
	matrix[6] = 0.0f;
	matrix[7] = -(top + bottom) / (top - bottom);

	matrix[8] = 0.0f;
	matrix[9] = 0.0f;
	matrix[10] = -2 / (far - near);
	matrix[11] = -(far + near) / (far - near);

	matrix[12] = 0.0f;
	matrix[13] = 0.0f;
	matrix[14] = 0.0f;
	matrix[15] = 1.0f;


	//result->coutMatrix();
	return *this;
}

Matrix_4x4 Matrix_4x4::multiply(float operand) {
	for (int i = 0; i < 16; i++) {
		matrix[i] = operand * matrix[i];
	}

	return *this;
}


Matrix_4x4 Matrix_4x4::multiply(Matrix_4x4 operand) {
	Matrix_4x4 result; 

	result.matrix[0] = matrix[0] * operand.matrix[0] + matrix[1] * operand.matrix[4] + matrix[2] * operand.matrix[8] + matrix[3] * operand.matrix[12];
	result.matrix[1] = matrix[0] * operand.matrix[1] + matrix[1] * operand.matrix[5] + matrix[2] * operand.matrix[9] + matrix[3] * operand.matrix[13];
	result.matrix[2] = matrix[0] * operand.matrix[2] + matrix[1] * operand.matrix[6] + matrix[2] * operand.matrix[10] + matrix[3] * operand.matrix[14];
	result.matrix[3] = matrix[0] * operand.matrix[3] + matrix[1] * operand.matrix[7] + matrix[2] * operand.matrix[11] + matrix[3] * operand.matrix[15];

	result.matrix[4] = matrix[4] * operand.matrix[0] + matrix[5] * operand.matrix[4] + matrix[6] * operand.matrix[8] + matrix[7] * operand.matrix[12];
	result.matrix[5] = matrix[4] * operand.matrix[1] + matrix[5] * operand.matrix[5] + matrix[6] * operand.matrix[9] + matrix[7] * operand.matrix[13];
	result.matrix[6] = matrix[4] * operand.matrix[2] + matrix[5] * operand.matrix[6] + matrix[6] * operand.matrix[10] + matrix[7] * operand.matrix[14];
	result.matrix[7] = matrix[4] * operand.matrix[3] + matrix[5] * operand.matrix[7] + matrix[6] * operand.matrix[11] + matrix[7] * operand.matrix[15];

	result.matrix[8] = matrix[8] * operand.matrix[0] + matrix[9] * operand.matrix[4] + matrix[10] * operand.matrix[8] + matrix[11] * operand.matrix[12];
	result.matrix[9] = matrix[8] * operand.matrix[1] + matrix[9] * operand.matrix[5] + matrix[10] * operand.matrix[9] + matrix[11] * operand.matrix[13];
	result.matrix[10] = matrix[8] * operand.matrix[2] + matrix[9] * operand.matrix[6] + matrix[10] * operand.matrix[10] + matrix[11] * operand.matrix[14];
	result.matrix[11] = matrix[8] * operand.matrix[3] + matrix[9] * operand.matrix[7] + matrix[10] * operand.matrix[11] + matrix[11] * operand.matrix[15];

	result.matrix[12] = matrix[12] * operand.matrix[0] + matrix[13] * operand.matrix[4] + matrix[14] * operand.matrix[8] + matrix[15] * operand.matrix[12];
	result.matrix[13] = matrix[12] * operand.matrix[1] + matrix[13] * operand.matrix[5] + matrix[14] * operand.matrix[9] + matrix[15] * operand.matrix[13];
	result.matrix[14] = matrix[12] * operand.matrix[2] + matrix[13] * operand.matrix[6] + matrix[14] * operand.matrix[10] + matrix[15] * operand.matrix[14];
	result.matrix[15] = matrix[12] * operand.matrix[3] + matrix[13] * operand.matrix[7] + matrix[14] * operand.matrix[11] + matrix[15] * operand.matrix[15];

	return result;
}

Vector_4 Matrix_4x4::multiplyByVector_4(Vector_4 o) {
	Vector_4 result;

	result.setX(matrix[0] * o.getX() + matrix[1] * o.getY() + matrix[2] * o.getZ() + matrix[3] * o.getW());
	result.setY(matrix[4] * o.getX() + matrix[5] * o.getY() + matrix[6] * o.getZ() + matrix[7] * o.getW());
	result.setZ(matrix[8] * o.getX() + matrix[9] * o.getY() + matrix[10] * o.getZ() + matrix[11] * o.getW());
	result.setW(matrix[12] * o.getX() + matrix[13] * o.getY() + matrix[14] * o.getZ() + matrix[15] * o.getW());
	
	return result;
}

Matrix_4x4 Matrix_4x4::createRotate2DMatrix(float angle, float centerx, float centery) {
	Matrix_4x4 mto; // translate 
	mto.matrix[3] = centerx;
	mto.matrix[7] = centery;
	mto.matrix[11] = 0;
	mto.matrix[0] = 1;
	mto.matrix[5] = 1;
	mto.matrix[10] = 1;
	mto.matrix[15] = 1;


	Matrix_4x4 m;  // z rotation... XY axis
	m.matrix[0] = cos(angle);
	m.matrix[1] = -sin(angle);
	m.matrix[4] = sin(angle);
	m.matrix[5] = cos(angle);
	m.matrix[10] = 1;
	m.matrix[15] = 1;

	Matrix_4x4 mb;
	mb.matrix[3] = -centerx;
	mb.matrix[7] = -centery;
	mb.matrix[11] = 0;
	mb.matrix[0] = 1;
	mb.matrix[5] = 1;
	mb.matrix[10] = 1;
	mb.matrix[15]= 1;


	Matrix_4x4 n = mto.multiply(m);
	Matrix_4x4 b = n.multiply(mb);

	return b;
}

Matrix_4x4 Matrix_4x4::createScaleMatrix(float scalex, float scaley, float scalez) {
	matrix[0] = scalex;
	matrix[5] = scaley;
	matrix[10] = scalez;
	matrix[15] = 1;
	return *this;
}

Matrix_4x4 Matrix_4x4::createRotateYMatrix(float angle) {
	matrix[0] = cos(angle);
	matrix[2] = -sin(angle);
	matrix[8] = sin(angle);
	matrix[10] = cos(angle);
	matrix[5] = 1;
	matrix[15] = 1;
	return *this;
}

Matrix_4x4 Matrix_4x4::createTranslateMatrix(float x, float y, float z) {
	matrix[3] = x;
	matrix[7] = y;
	matrix[11] = z;
	matrix[0] = 1;
	matrix[5] = 1;
	matrix[10] = 1;
	matrix[15] = 1;
	return *this;
}

Matrix_4x4 Matrix_4x4::createFrustrumMatrix(float left, float right, float bottom, float top, float near, float far) {
	matrix[0] = (2.0 * near) / (right - left);
	matrix[2] = (right + left) / (right - left);
	matrix[5] = (2.0 * near) / (top - bottom);
	matrix[6] = (top + bottom) / (top - bottom);
	matrix[10] = -(far + near) / (far - near);
	matrix[11] = -2.0 * far * near / (far - near);
	matrix[14] = -1.0;
	return *this;
}

Matrix_4x4 Matrix_4x4::frustrum(float left, float right, float bottom, float top, float near, float far) {
	matrix[0] = near;
	matrix[1] = 0.0f;
	matrix[2] = 0.0f;
	matrix[3] = 0.0f;

	matrix[4] = 0.0f;
	matrix[5] = near;
	matrix[6] = 0.0f;
	matrix[7] = 0.0f;

	matrix[8] = 0.0f;
	matrix[9] = 0.0f;
	matrix[10] = far+near;
	matrix[11] = -1.0f;

	matrix[12] = 0.0f;
	matrix[13] = 0.0f;
	matrix[14] = far*near;
	matrix[15] = 0.0f;

	return *this;
}


void Matrix_4x4::coutMatrix() {
	cout << "MATRIX " << endl;;
	for (size_t i = 0; i < 16; i++)
	{
		if (i % 4 == 0) cout << endl;
		cout << matrix[i] << "  ";
	}
}

Matrix_4x4 Matrix_4x4::createMatrix(const float* m) {
	matrix[0] = m[0];
	matrix[1] = m[1];
	matrix[2] = m[2];
	matrix[3] = m[3];

	matrix[4] = m[4];
	matrix[5] = m[5];
	matrix[6] = m[6];
	matrix[7] = m[7];

	matrix[8] = m[8];
	matrix[9] = m[9];
	matrix[10] = m[10];
	matrix[11] = m[11];

	matrix[12] = m[12];
	matrix[13] = m[13];
	matrix[14] = m[14];
	matrix[15] = m[15];

	return *this;
}

Matrix_4x4 Matrix_4x4::createTransposedMatrix(const float* m) {
	matrix[0] = m[0];
	matrix[1] = m[4];
	matrix[2] = m[8];
	matrix[3] = m[12];
				
	matrix[4] = m[1];
	matrix[5] = m[5];
	matrix[6] = m[9];
	matrix[7] = m[13];
				
	matrix[8] = m[2];
	matrix[9] = m[6];
	matrix[10] = m[10];
	matrix[11] = m[14];
				 
	matrix[12] = m[3];
	matrix[13] = m[7];
	matrix[14] = m[11];
	matrix[15] = m[15];

	return *this;
}

Matrix_4x4 Matrix_4x4::testMatrix() {
	matrix[0] = 1;
	matrix[1] = 0;
	matrix[2] = 0;
	matrix[3] = 0;

	matrix[4] = 0;
	matrix[5] = 2;
	matrix[6] = 0;
	matrix[7] = 0;

	matrix[8] = 0;
	matrix[9] = 0;
	matrix[10] = 3;
	matrix[11] = 0;

	matrix[12] = 0;
	matrix[13] = 0;
	matrix[14] = 0;
	matrix[15] = 4;

	return *this;
}

float** Matrix_4x4::create2DimensionalMatrix(float* matrix) {
	float** m = new float*[4];
	for (uint32_t i = 0; i < 4; ++i) {
		m[i] = new float[4];
	}
	int inc = 0;
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			m[i][j] = matrix[inc];
			inc++;
		}
	}
	return m;
}



int Matrix_4x4::isRegular(int sizeOfMatrix) {
	float** x = create2DimensionalMatrix(matrix);

	/*for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			cout << x[i][j] << endl;
		}
	}*/
	
	int indxc[4], indxr[4], ipiv[4];
	int i, icol, irow, j, k, l, ll, n;
	float big, dum, pivinv, temp;
	
	// satisfy the compiler
	icol = irow = 0;

	// the size of the matrix
	n = sizeOfMatrix;

	for (j = 0; j < n; j++) /* zero pivots */
	
		ipiv[j] = 0;

	for (i = 0; i < n; i++)
	{
		big = 0.0;
		for (j = 0; j < n; j++)
			if (ipiv[j] != 1)
				for (k = 0; k<n; k++)
				{
					if (ipiv[k] == 0)
					{
						if (fabs(x[k][j]) >= big)
						{
							big = fabs(x[k][j]);
							irow = j;
							icol = k;
						}
					}
					else
						if (ipiv[k] > 1)
							return 1; /* singular matrix */
							
				}
		++(ipiv[icol]);
		if (irow != icol)
		{
			for (l = 0; l<n; l++)
			{
				temp = x[l][icol];
				x[l][icol] = x[l][irow];
				x[l][irow] = temp;
			}
		}
		indxr[i] = irow;
		indxc[i] = icol;
		if (x[icol][icol] == 0.0)
			return 1; /* singular matrix */
			
		pivinv = 1.0 / x[icol][icol];
		x[icol][icol] = 1.0;
		for (l = 0; l<n; l++)
			x[l][icol] = x[l][icol] * pivinv;

		for (ll = 0; ll < n; ll++)
			if (ll != icol)
			{
				dum = x[icol][ll];
				x[icol][ll] = 0.0;
				for (l = 0; l<n; l++)
					x[l][ll] = x[l][ll] - x[l][icol] * dum;
			}
	}
	for (l = n; l--; )
	{
		if (indxr[l] != indxc[l])
			for (k = 0; k<n; k++)
			{
				temp = x[indxr[l]][k];
				x[indxr[l]][k] = x[indxc[l]][k];
				x[indxc[l]][k] = temp;
			}
	}

	return 0; // matrix is regular .. inversion has been succesfull
	
}



Matrix_4x4 Matrix_4x4::adjoint() {
	double m[16];
	Matrix_4x4 invm;
	
	double inv[16];
	for (int i = 0; i < 16; i++) {
		m[i] = matrix[i];
	}
	// a
	inv[0] = m[5] * m[10] * m[15] -
		m[5] * m[11] * m[14] -
		m[9] * m[6] * m[15] +
		m[9] * m[7] * m[14] +
		m[13] * m[6] * m[11] -
		m[13] * m[7] * m[10];

	inv[4] = -m[4] * m[10] * m[15] +
		m[4] * m[11] * m[14] +
		m[8] * m[6] * m[15] -
		m[8] * m[7] * m[14] -
		m[12] * m[6] * m[11] +
		m[12] * m[7] * m[10];

	inv[8] = m[4] * m[9] * m[15] -
		m[4] * m[11] * m[13] -
		m[8] * m[5] * m[15] +
		m[8] * m[7] * m[13] +
		m[12] * m[5] * m[11] -
		m[12] * m[7] * m[9];

	inv[12] = -m[4] * m[9] * m[14] +
		m[4] * m[10] * m[13] +
		m[8] * m[5] * m[14] -
		m[8] * m[6] * m[13] -
		m[12] * m[5] * m[10] +
		m[12] * m[6] * m[9];

	inv[1] = -m[1] * m[10] * m[15] +
		m[1] * m[11] * m[14] +
		m[9] * m[2] * m[15] -
		m[9] * m[3] * m[14] -
		m[13] * m[2] * m[11] +
		m[13] * m[3] * m[10];

	inv[5] = m[0] * m[10] * m[15] -
		m[0] * m[11] * m[14] -
		m[8] * m[2] * m[15] +
		m[8] * m[3] * m[14] +
		m[12] * m[2] * m[11] -
		m[12] * m[3] * m[10];

	inv[9] = -m[0] * m[9] * m[15] +
		m[0] * m[11] * m[13] +
		m[8] * m[1] * m[15] -
		m[8] * m[3] * m[13] -
		m[12] * m[1] * m[11] +
		m[12] * m[3] * m[9];

	inv[13] = m[0] * m[9] * m[14] -
		m[0] * m[10] * m[13] -
		m[8] * m[1] * m[14] +
		m[8] * m[2] * m[13] +
		m[12] * m[1] * m[10] -
		m[12] * m[2] * m[9];

	inv[2] = m[1] * m[6] * m[15] -
		m[1] * m[7] * m[14] -
		m[5] * m[2] * m[15] +
		m[5] * m[3] * m[14] +
		m[13] * m[2] * m[7] -
		m[13] * m[3] * m[6];

	inv[6] = -m[0] * m[6] * m[15] +
		m[0] * m[7] * m[14] +
		m[4] * m[2] * m[15] -
		m[4] * m[3] * m[14] -
		m[12] * m[2] * m[7] +
		m[12] * m[3] * m[6];

	inv[10] = m[0] * m[5] * m[15] -
		m[0] * m[7] * m[13] -
		m[4] * m[1] * m[15] +
		m[4] * m[3] * m[13] +
		m[12] * m[1] * m[7] -
		m[12] * m[3] * m[5];

	inv[14] = -m[0] * m[5] * m[14] +
		m[0] * m[6] * m[13] +
		m[4] * m[1] * m[14] -
		m[4] * m[2] * m[13] -
		m[12] * m[1] * m[6] +
		m[12] * m[2] * m[5];

	inv[3] = -m[1] * m[6] * m[11] +
		m[1] * m[7] * m[10] +
		m[5] * m[2] * m[11] -
		m[5] * m[3] * m[10] -
		m[9] * m[2] * m[7] +
		m[9] * m[3] * m[6];

	inv[7] = m[0] * m[6] * m[11] -
		m[0] * m[7] * m[10] -
		m[4] * m[2] * m[11] +
		m[4] * m[3] * m[10] +
		m[8] * m[2] * m[7] -
		m[8] * m[3] * m[6];

	inv[11] = -m[0] * m[5] * m[11] +
		m[0] * m[7] * m[9] +
		m[4] * m[1] * m[11] -
		m[4] * m[3] * m[9] -
		m[8] * m[1] * m[7] +
		m[8] * m[3] * m[5];

	inv[15] = m[0] * m[5] * m[10] -
		m[0] * m[6] * m[9] -
		m[4] * m[1] * m[10] +
		m[4] * m[2] * m[9] +
		m[8] * m[1] * m[6] -
		m[8] * m[2] * m[5];


	for (int i = 0; i < 16; i++) {
		invm.matrix[i] = inv[i];
	}
	return invm;
}

Matrix_4x4 Matrix_4x4::inverse() {

	Matrix_4x4 inv = adjoint();

	//inv.coutMatrix();
	

	float det = matrix[0] * inv.matrix[0] + matrix[1] * inv.matrix[4] + matrix[2] * inv.matrix[8] + matrix[3] * inv.matrix[12];

	if (!det) {
		cout << "CANNOT MAKE INVERSE" << endl;
		return *this;
	}

	det = 1.0 / det;

	inv = inv.multiply(det);

	/*for (int i = 0; i < 16; ++i)
		inv.matrix[i] = inv.matrix[i] * det;*/

	return inv;
}



//
//Vector_3 Matrix_4x4::multiplyVector_3ByMatrix(Vector_3 v) {
//	Vector_3 v1;
//	float x1 = v.getX();
//	float y1 = v.getY();
//	float z1 = v.getZ();
//
//	v1.setX(x1 * matrix[0] + y1 * matrix[1] + z1 * matrix[2]);// +0 * matrix[3];
//	v1.setY(x1 * matrix[4] + y1 * matrix[5] + z1 * matrix[6]);// +0 * matrix[7];
//	v1.setZ(x1 * matrix[8] + y1 * matrix[9] + z1 * matrix[10]);// +0 * matrix.matrix[11];
//
//	return v1;
//}
//
//
//Vector_4 Matrix_4x4::multiplyVector_4ByMatrix(Vector_4 v) {
//	Vector_4 v1;
//	float x1 = v.getX();
//	float y1 = v.getY();
//	float z1 = v.getZ();
//	float w1 = v.getW();
//	
//	v1.setX(x1 * matrix[0] + y1 * matrix[1] + z1 * matrix[2]+ w1 * matrix[3]);
//	v1.setY(x1 * matrix[4] + y1 * matrix[5] + z1 * matrix[6] + w1 * matrix[7]);
//	v1.setZ(x1 * matrix[8] + y1 * matrix[9] + z1 * matrix[10] + w1 * matrix[11]);
//	v1.setW(x1 * matrix[12] + y1 * matrix[13] + z1 * matrix[14] + w1 * matrix[15]);
//	
//	return v1;
//}

