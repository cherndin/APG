﻿#include "RayTracer.h"


void RayTracer::setMVP(Matrix_4x4 m) {
	MVP = m;
	MVPinverse = m.inverse();
}

void RayTracer::setViewport(Matrix_4x4 m) {
	viewport = m;
	viewportInverse = m.inverse();
}

Color RayTracer::getEnvironmentColor(Ray ray) {
	if (envMap != nullptr) {
		float distance = sqrt(ray.direction.getX() * ray.direction.getX() + ray.direction.getY() * ray.direction.getY());
		float rad = distance > 0 ? acos(ray.direction.getZ()) / (distance * 2 * M_PI) : 0;

		float u = 0.5 + ray.direction.getX() * rad;
		float v = 0.5 + ray.direction.getY() * rad;

		Color c = envMap->getColor(u, v);

		return c;
	}
	else {
		return backgroundColor;
	}
}

Color RayTracer::castRay(Ray ray) {
	Color color = Color(0, 0, 0); // getBackgroudColor(ray);

	Primitive *hit = nullptr;
	float minDistance = FLOAT_MAX_VALUE;

	//find the nearest primitive
	for (vector<Primitive*>::iterator it = primitives.begin(); it != primitives.end(); ++it) {
		Primitive* primitive = *it;
		float tHit = 0;
		bool intersects = intersectRayWithPrimitive(ray, primitive, tHit);
		if (intersects) {
			if (tHit < minDistance && tHit > 0.1) {
				hit = primitive;
				minDistance = tHit;
			}
		}
	}


	if (hit) {
		// find an intersection with a primitive
		Vector_3 intersection = ray.origin.add(ray.direction.multiplyByFloat(minDistance));
		Vector_3 normal;

		// find a normal
		if (Sphere* sph = dynamic_cast<Sphere*>(hit)) {
			normal = sph->getNormalOfSphere(minDistance, ray);
		}
		else {
			normal = hit->getNormal();
		}

		if (hit->areaLight) {
			color.add(hit->getMaterial().color);
		}
		else {
			// go through point lights and calculate color using phong
			for (auto it = pointLights.begin(); it != pointLights.end(); ++it) {
				PointLight light = *it;

				Vector_3 lightDir = intersection.subtract(light.getPosition()).normalize();

				float tmax = light.getPosition().subtract(intersection).length();
				Ray rayFromLightToPrim(light.getPosition(), lightDir, 0, 0.0, tmax);

				float distanceToOrigPrim = 0;
				//find the intersection
				intersectRayWithPrimitive(rayFromLightToPrim, hit, distanceToOrigPrim);

				// is light in shadow
				if (isInShadow(rayFromLightToPrim, distanceToOrigPrim)) {
					color = Color(0, 0, 0);
				}
				else {
					Color c = light.calculatePhongLight(hit->getMaterial(), intersection, ray.origin, normal);
					color.add(c);
				}
			}

			// go through area lights
			for (auto it = areaLights.begin(); it != areaLights.end(); ++it) {
				AreaLight* light = *it;

				// go through all triangles and find intersection
				for (auto tr = light->triangles.begin(); tr != light->triangles.end(); ++tr) {
					Triangle* triangle = *tr;

					float triangleArea = triangle->getArea();
					Vector_3 triangleNormal = triangle->getNormal();
					Color lightColor = light->material.color;

					Color primColor = hit->getMaterial().color;
					float primDiffuse = hit->getMaterial().diffuse;

					for (int i = 0; i < NUM_SAMPLES_AREA_LIGHT; i++) {
						Vector_3 randomTrianglePoint = triangle->getRandomPoint();
						Vector_3 toPrim = intersection.subtract(randomTrianglePoint);

						float distanceFromLightToPrim = floor(toPrim.length());

						toPrim = toPrim.normalize();
						Ray areaLightRay(randomTrianglePoint, toPrim);


						// is light in shadow
						if (isInShadow(areaLightRay, distanceFromLightToPrim)) {
							continue;
						}
						else {
							// create a point light out of the area light ray
							PointLight pLight(light->material.color, randomTrianglePoint);
							float cosFi = fabs(triangleNormal.dotProduct(toPrim));
							float weirdThing = light->getWeirdThing(distanceFromLightToPrim);
							float sampleVal = (triangleArea*cosFi) / (NUM_SAMPLES_AREA_LIGHT*weirdThing);

							Color c = pLight.calculatePhongLight(hit->getMaterial(), intersection, ray.origin, normal).multiply(sampleVal);
							color.add(c);
						}
					}
				}
			}

		}
		// sends ray further depending on a prim material
		color.add(sendRayFurther(ray, hit, normal, intersection));	
	} else {
		// if nothing was hit then get the environment/background color
		return getEnvironmentColor(ray);
	}
	return color;
}


Color RayTracer::sendRayFurther(Ray ray, Primitive* hit, Vector_3 normal, Vector_3 intersection) {

	Color color;

	// if ray depth is less than 8
	if (ray.depth < DEPTH) {

		// find the color of reflected rays
		if (hit->getMaterial().specular > 0) {

			//angle between a normal and a ray
			float angleNormalRay = normal.dotProduct(ray.direction.multiplyByFloat(-1.0));

			// direction of a reflected ray
			Vector_3 reflRayDir = ray.direction.add(normal.multiplyByFloat(2 * angleNormalRay)).normalize();

			// origin of a reflected ray
			Vector_3 reflRayOr = intersection.add(reflRayDir.multiplyByFloat(shift));

			Ray reflectedRay(reflRayOr, reflRayDir, ray.depth + 1);
			// cast ray further
			Color rayColor = castRay(reflectedRay);
			color.add(rayColor.multiply(hit->getMaterial().specular));
		}

		// find the color of refracted rays
		if (hit->getMaterial().transmittence > 0) {

			Ray refractedRay;
			refractedRay.depth = ray.depth + 1;

			bool refracted = refractedRay.isRefracted(ray, normal, hit->getMaterial().refraction);

			if (refracted) {
				// origin of a refracted ray
				refractedRay.origin = intersection.add(ray.direction.multiplyByFloat(shift));
				// cast ray further
				Color rayColor = castRay(refractedRay);
				color.add(rayColor.multiply(hit->getMaterial().transmittence));
			}
		}
	}

	return color;
}

void RayTracer::addPrimitive(Primitive* p) {
	primitives.push_back(p);
}

void RayTracer::addPointLight(PointLight* light) {
	pointLights.push_back(*light);
}

void RayTracer::addAreaLight(AreaLight* light) {
	areaLights.push_back(light);
}

void RayTracer::setBackgroundColor(Color color) {
	backgroundColor = color;
}

Color RayTracer::getBackgroundColor() {
	return backgroundColor;
}

void RayTracer::setEnvironmentMap(EnvironmentMap* map) {
	envMap = map;
}

bool RayTracer::isInShadow(Ray rayFromLightToPrim, float distanceToOriginalPrim) {
	for (vector<Primitive*>::iterator it = primitives.begin(); it != primitives.end(); ++it) {
		Primitive* primitive = *it;
		float tHit = 0;
		bool intersects = intersectRayWithPrimitive(rayFromLightToPrim, primitive, tHit);

		if (intersects) {
			//if current distance is lower comparing to the original primitive distance then the primitive is in shadow
			if (tHit < distanceToOriginalPrim) {
				return true;
			}
		}
	}
	return false;
}

bool RayTracer::intersectRayWithPrimitive(Ray ray, Primitive* p, float &tHit) {
	bool intersects = false;
	if (Triangle* trg = dynamic_cast<Triangle*>(p)) {
		intersects = trg->intersect(ray, tHit);
	}
	else if (Sphere* sph = dynamic_cast<Sphere*>(p)) {
		intersects = sph->intersect(ray, tHit);
	}

	return intersects;
}

void RayTracer::clearLights() {
	pointLights.clear();
	areaLights.clear();
}



