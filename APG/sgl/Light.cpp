#include "Light.h"

void PointLight::setPosition(Vector_3 position) {
	PointLight::position = position;
}

Vector_3 PointLight::getPosition() {
	return position;
}

Color PointLight::getColor() {
	return color;
}


Color PointLight::calculatePhongLight(Material material, Vector_3 positionSurface, Vector_3 positionCamera,	Vector_3 normal) {
	Color fragmentColor;

	Vector_3 toCamera = positionCamera.subtract(positionSurface).normalize();
	Vector_3 toLight = getPosition().subtract(positionSurface).normalize();
	Vector_3 fromLight = positionSurface.subtract(getPosition()).normalize(); 
	//Vector_3 fromCamera = positionSurface.subtract(positionCamera).normalize(); 

	float intensity = normal.dotProduct(toLight);
	
	Vector_3 relflected = fromLight.subtract(normal.multiplyByFloat(2*normal.dotProduct(fromLight))).normalize();

	if (intensity > 0.0) {	
		// Diffuse
		float maxAngleDiffuse = max(toLight.dotProduct(normal), 0.0f);
		Color diffuse = material.color.multiply(getColor()).multiply(maxAngleDiffuse  * material.diffuse);
		fragmentColor.add(diffuse);

		//// Specular
		float angleCosinus = toCamera.dotProduct(relflected);
		float tempCoeficient = material.specular * pow(max(angleCosinus, 0.0f), material.shininess);
		Color specular = getColor().multiply(tempCoeficient);
		fragmentColor.add(specular);
	}

	return fragmentColor;
}


void AreaLight::addTriangle(Triangle* t) {
	triangles.push_back(t);
}



AreaLight::AreaLight(EmissiveMaterial material) : material(material) {

}

float AreaLight::getWeirdThing(float d) {
	return material.constant0 + d*material.constant1 + d*d*material.constant2;
}
