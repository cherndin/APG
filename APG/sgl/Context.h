#ifndef CONTEXT_H
#define CONTEXT_H

#include "Color.h"
#include "Vertex.h"
#include "Edge.h"
#include "Pixel.h"
#include "Matrix_4x4.h"
#include "RayTracer.h"
#include "UsefulValues.h"
#include "Primitive.h"
#include "Material.h"
#include "sgl.h"

#include <vector>
#include <limits>
#include <list>
#include <algorithm>
#include <stack>
#include <cmath>  

#include <math.h>

using namespace std;

class Context {

public:

	/**
	* Context constructor
	* @param[in] width window width
	* @param[in] height window height
	*/
	Context(uint64_t width, uint64_t height);

	~Context(){};

	/**
	* Sets color on pixel on the screen
	* @param[in] color 
	* @param[in] pixel 
	*/
	void setColorOnPixel(Color color, Pixel pixel);

	/**
	* Gets pointer to the first member of color buffer list
	* @return poiner
	*/
	float* getColorBufferPointer();


	/**
	* Sets clear color
	* @param[in] color clear color
	*/
	void setClearColor(Color color);


	/**
	* Gets clear color
	* @return color
	*/
	Color getClearColor();

	/**
	* Draws arc. Depending on areaMode arc is filled with specific color or not.
	* Arc is made of connected vertices - polygons.
	* @param[in] v vertex
	* @param[in] radius radius of arc
	* @param[in] from angle where arc starts
	* @param[in] to angle where arc ends
	*/
	void drawArc(Vertex v, float radius, float from, float to);


	/**
	* Draws arc. Depending on areaMode arc is filled with specific color or not.
	* Arc is made of connected vertices - polygons.
	* @param[in] v vertex
	* @param[in] radius radius of arc
	* @param[in] from angle where arc starts
	* @param[in] to angle where arc ends
	* @param[in] fill if arc is filled with color
	*/
	void drawArc(Vertex v, float radius, float from, float to, bool fill);


	/**
	* Gets scale of the scene
	* @return scale
	*/
	float getScale();	


	/**
	* Sets current color used for drawing and filling
	* @param[in] color current color
	*/
	void setCurrentColor(Color color);


	/**
	* Gets current color used for drawing and filling
	* @return current color
	*/
	Color getCurrentColor();

	/**
	* Sets depth test
	* @param[in] value is set or not
	*/
	void setDepthTest(bool value);
	
	/**
	* Gets depth test
	* @return boolean is set or not
	*/
	bool getDepthTest();

	/**
	* Clears color buffer array
	*/
	void clearColorBuffer();

	/**
	* Gets depth test
	* @return boolean is set or not
	*/
	bool isBusy();

	/**
	* Sets context busy when drawing primitive objects
	* @param[in] value is set or not
	*/
	void setBusy(bool value);


	/**
	* Sets drawing mode
	* @param[in] mode 
	*/
	void setDrawingMode(sglEElementType mode);

	/**
	* Gets drawing mode
	* @return drawing mode
	*/
	sglEElementType getDrawingMode();

	/**
	* Gets point size with which the primitive objects are drawn
	* @return point size
	*/
	void setPointSize(float value);

	/**
	* Gets point size with which the primitive objects are drawn
	* @return point size
	*/
	float getPointSize();

	/**
	* Adds vertex to a vertex list
	* @param[in] vertex
	*/
	void addVertex(Vertex vertex);

	/**
	* Clears vertex buffer array
	*/
	void clearVertexBuffer();

	/**
	* Sets pixel with current color
	* @param[in] x
	* @param[in] y
	* @param[in] z
	*/
	void setPixel(float x, float y, float z = 0);

	/**
	* Depending on drawing mode draws a primitive object 
	*/
	void draw();

	/**
	* Draws a point
	*/
	void rasterizePoints();

	/**
	* Draws lines depending on a specific line type 
	* @param[in] mode line type 
	*/
	void rasterizeLines(sglEElementType mode);

	/**
	* Draws a line between 2 verticies
	* @param[in] v1 first vertex
	* @param[in] v2 second vertex
	*/
	void rasterizeLine(Vertex v1, Vertex v2);

	/**
	* Sets matrix mode(MV, projection)
	* @param[in] mode matrix mode
	*/
	void setMatrixMode(sglEMatrixMode mode);

	/**
	* Gets current matrix mode
	* @return matrix mode
	*/
	sglEMatrixMode getMatrixMode();

	/**
	* Sets viewport matrix
	* @param[in] x 
	* @param[in] y
	* @param[in] width
	* @param[in] height
	*/
	void setViewportMatrix(uint64_t x, uint64_t y, uint64_t width, uint64_t height);

	/**
	* Gets viewport matrix
	* @return viewport matrix
	*/
	Matrix_4x4 getViewportMatrix();

	/**
	* Sets viewport matrix
	* @param[in] mode which matrix to set(MV, projection)
	* @param[in] matrix what to set it to
	*/
	void setMatrix(sglEMatrixMode mode, Matrix_4x4 matrix);

	/**
	* Gets matrix depending on a matrix mode(MV, projection)
	* @return viewport matrix
	*/
	Matrix_4x4 getMatrix(sglEMatrixMode mode);

	/**
	* Gets last transform matrix
	* @return matrix
	*/
	Matrix_4x4 getTopTransform();	
	
	/**
	* Gets transform stack size
	* @return size
	*/
	int32_t getTransformationStackSize();

	/**
	* Pushes an identity matrix to the matrix stack
	*/
	void addIdentityToTransformationStack();

	/**
	* Multiplies a matrix depending on a matrix mode with ortho projection matrix
	* @param[in] left
	* @param[in] right
	* @param[in] bottom
	* @param[in] top
	* @param[in] near
	* @param[in] far
	*/
	void orthoMultiplication(float left, float right, float bottom, float top, float near, float far);


	/**
	* Draws a circle
	* @param[in] v center
	* @param[in] r radius
	* @param[in] fill boolean will be filled with current color
	*/
	void drawCircle(Vertex v, float r, bool fill);

	/**
	* Draws a circle
	* @param[in] v center
	* @param[in] r radius
	*/		
	void drawCircle(Vertex v, float r);


	/**
	* Sets symetril pixels when drawing circle
	* @param[in] x
	* @param[in] y
	* @param[in] xc x shift
	* @param[in] yc y shift
	*/
	void setSymPixels(int x, int y, int xc, int yc);

	/**
	* Draws ellipse
	* @param[in] v center
	* @param[in] a 1st radius
	* @param[in] b 2nd radius
	*/
	void drawEllipse(Vertex v, float a, float b);

	/**
	* Draws ellipse
	* @param[in] v center
	* @param[in] a 1st radius
	* @param[in] b 2nd radius
	* @param[in] fill boolean will be filled with current color
	*/
	void drawEllipse(Vertex v, float a, float b, bool fill);

	/**
	* Sets area mode
	* @param[in] mode area mode
	*/
	void setAreaMode(sglEAreaMode mode);

	/**
	* Fills a polygon with color
	*/
	void rasterizeFilledPolygon();

	/**
	* Interpolation of z coords of a line 
	* @param[in] yBottom1 y bottom of 1st line
	* @param[in] yTop1 y top of 1st line
	* @param[in] yBottom2 y bottom of 2nd line
	* @param[in] yTop2 y top of 2nd line
	* @param[in] zBottom1 z bottom of 1st line
	* @param[in] zTop1 z top of 1st line
	* @param[in] zBottom2 z bottom of 2nd line
	* @param[in] zTop2 z top of 2nd line
	* @param[in] from where from to fill the line
	* @param[in] to to where to fill the line
	* @param[in] y y coords of the line. line is horizontal.
	*/
	void fill(float yBottom1, float yTop1, float yBottom2, float yTop2, float zBottom1, float zTop1, float zBottom2, float zTop2, float from, float to, float y);

	/**
	* Clears depth buffer
	*/
	void clearDepthBuffer();

	/**
	* Multiplies a matrix depending on a matrix mode with frustrum projection matrix
	* @param[in] left center
	* @param[in] right
	* @param[in] bottom
	* @param[in] top
	* @param[in] near
	* @param[in] far
	*/
	void frustrumMultiplication(float left, float right, float bottom, float top, float near, float far);

	/**
	* Multiplies a matrix depending on a matrix mode with given matrix
	* @param[in] m matrix
	*/
	void matrixMultiplication(Matrix_4x4 m);

	/**
	* Fills a horizontal line with current color
	* @param[in] x1 start x
	* @param[in] x2 end x
	* @param[in] y
	*/
	void fillBetweenTwoPoints(float x1, float x2, float y);

	/**
	* Fills a horizontal line with current color
	* @param[in] x1 start x
	* @param[in] x2 end x
	* @param[in] y
	* @param[in] d_a z of 1st point
	* @param[in] d_b z of 2nd point
	*/
	void fillBetweenTwoPoints(float x_a, float x_b, int64_t y, float d_a, float d_b);		  
	

	struct sortVals : public std::binary_function<float, float, bool>
	{
		bool operator()(float a, float b)
		{
			return a < b;
		}
	};

	struct sortEdges : public std::binary_function<Edge, Edge, bool>
	{
		bool operator()(Edge a, Edge b)
		{
			return a.intersection < b.intersection;
		}
	};


	/**
	* Is scene being defined by ray tracer
	* @return boolean is scene being defined
	*/
	bool isBeingDefined();

	/**
	* Sets scene definition by ray tracer
	* @param[in] statement boolean
	*/
	void setSceneDefinition(bool statement);

	/**
	* Adds triangle depending if areaLightsMode is set. If yes, then adds a triangle to areaLights, if not to vector buffer
	*/
	void addTriangle();

	/**
	* Adds sphere
	* @param[in] x
	* @param[in] y
	* @param[in] z
	* @param[in] radius
	*/
	void addSphere(float x, float y, float z, float radius);

	/**
	* Sets current material
	* @param[in] r
	* @param[in] g
	* @param[in] b
	* @param[in] kd
	* @param[in] ks
	* @param[in] shine
	* @param[in] T
	* @param[in] ior
	*/
	void setCurrentMaterial( float r,  float g, float b,float kd, float ks, float shine, float T, float ior);

	/**
	* Adds a point light
	* @param[in] p point light
	*/
	void addPointLight(PointLight* p);

	///**
	//* Adds an area light
	//* @param[in] p area light
	//*/
	//void addAreaLight(AreaLight* p);

	/**
	* Creates a ray and casts to the scene from all the pixels on the screen
	*/
	void renderRayTracerScene();

	/**
	* Sets an environment map
	* @param[in] width
	* @param[in] height
	* @param[in] texels
	*/
	void setEnvironmentMap( int width, int height, float *texels);

	/**
	* Sets an emissive material and adds a new areaLight to a ray tracer
	* @param[in] r
	* @param[in] g
	* @param[in] b
	* @param[in] c0
	* @param[in] c1
	* @param[in] c2
	*/
	void setCurrentEmissiveMaterial( float r, float g, float b, float c0, float c1, float c2);


	/**
	* Adds vector to a vector buffer
	* @param[in] v vector
	*/
	void addVector(Vector_3 v);

	/**
	* Clears vector buffer
	*/
	void clearVectorBuffer();

	/**
	* Draws area lights
	*/
	//void drawAreaLights();




public:
	
	uint64_t width, height, windowSize, counter;

	Color clearColor;

	Color currentColor;

	Color* colorBuffer;

	bool depthTest;

	vector<Vertex> vertexBuffer;

	list<Matrix_4x4> transformationStack;

	bool busy;

	sglEElementType drawingMode;

	float pointSize;

	sglEMatrixMode matrixMode;

	Matrix_4x4 viewportMatrix;

	// MVP

	Matrix_4x4 projectionMatrix;

	Matrix_4x4 modelViewMatrix;

	sglEAreaMode areaMode;

	float* depthBuffer;

	vector<Vector_3> vectorBuffer;	
	
	vector<Edge> edges;

	RayTracer rayTracer;

	bool defining;

	float near, far;

	Material material;
	
	EmissiveMaterial emissiveMaterial;

	AreaLight* areaLight;

	bool areaLightsMode;



};


#endif