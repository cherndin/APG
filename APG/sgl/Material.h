#pragma once

#ifndef MATERIAL_H
#define MATERIAL_H

#include "Color.h"

class Material {

public:
	Material() {};
	Material(const float r,
		const float g,
		const float b,
		const float kd,
		const float ks,
		const float shine,
		const float T,
		const float ior);

	Material(Color c);


	Color color;

	float diffuse;
	float specular;
	float shininess;
	float transmittence;
	float refraction;
};

class EmissiveMaterial {
public:
	EmissiveMaterial() {};
	EmissiveMaterial(const float r,
		const float g,
		const float b,
		const float c0,
		const float c1,
		const float c2);

	Color color;
	float constant0;
	float constant1;
	float constant2;

};

#endif