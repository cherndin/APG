#ifndef CONTEXT_CONTAINER_H
#define CONTEXT_CONTAINER_H

#include <vector>

#include "Context.h"


class ContextContainer {

public:

	ContextContainer() {

	}
	~ContextContainer() {

	}

	int add(Context* context); // returns context id

	void remove(int64_t id);

	void setCurrent(int64_t id);

	Context* getCurrent();

	int64_t getContextId();

	int64_t getSize();

	bool isEmpty();



private:
	std::vector<Context*> container; // list of pointers
	Context* current;
};


#endif