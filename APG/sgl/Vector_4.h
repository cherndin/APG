#pragma once
#ifndef VECTOR_4_H
#define VECTOR_4_H

#include <limits>
#include <iostream>

//#include "Vertex.h"
#include "Vector_3.h"
//#include "Matrix_4x4.h"

using namespace std;


class Vector_4 {
public:

	Vector_4();
	Vector_4(float x, float y, float z, float w);
	//Vector_4(Vertex v);

	void setX(float value);
	void setY(float value);
	void setZ(float value);
	void setW(float value);


	float getX();
	float getY();
	float getZ();
	float getW();


	Vector_4 normalize(); // Zdroj : http://www.fundza.com/vectors/normalize/

	Vector_4 subtract(Vector_4 v);

	Vector_3 divideByW();

	void coutV() {
		cout << x << " " << y << " " << z << " " << w <<  endl;
	};


private:
	float x, y, z, w;
};

#endif