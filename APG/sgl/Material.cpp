#include "Material.h"

Material::Material(const float r,
	const float g,
	const float b,
	const float kd,
	const float ks,
	const float shine,
	const float T,
	const float ior) {

	color = Color(r, g, b);
	diffuse = kd;
	specular = ks;
	shininess = shine;
	transmittence = T;
	refraction = ior;
	}

Material::Material(Color c):color(c) {
	diffuse = 1;
	specular = 0;
	shininess = 0;
	transmittence = 0;
	refraction = 0;

}


EmissiveMaterial::EmissiveMaterial(const float r,
	const float g,
	const float b,
	const float c0,
	const float c1,
	const float c2) {

	color = Color(r, g, b);

	constant0 = c0;
	constant1 = c1;
	constant2 = c2;
}
