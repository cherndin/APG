#include "Vector_4.h"

Vector_4::Vector_4() {
	x = 0.0f;
	y = 0.0f;
	z = 0.0f;
}

//Vector_4::Vector_4(Vertex v) {
//	x = v.getX();
//	y = v.getY();
//	z = v.getZ();
//}

Vector_4::Vector_4(float x, float y, float z, float w) : x(x), y(y), z(z), w(w) {

}
void Vector_4::setX(float value) { x = value; }
void Vector_4::setY(float value) { y = value; }
void Vector_4::setZ(float value) { z = value; }
void Vector_4::setW(float value) { w = value; }



float Vector_4::getX() { return x; }
float Vector_4::getY() { return y; }
float Vector_4::getZ() { return z; }
float Vector_4::getW() { return w; }



Vector_4 Vector_4::normalize() {
	float size = sqrt(x*x + y*y + z*z + w*w);

	if (size == 0 || size == 1)
		return *this;

	x /= size;
	y /= size;
	z /= size;
	w /= size;

	return *this;
}

Vector_4 Vector_4::subtract(Vector_4 v) {
	return Vector_4(x - v.getX(), y - v.getY(), z - v.getZ(), w - v.getW());
}



Vector_3 Vector_4::divideByW() {
	return Vector_3(x / w, y / w, z / w);
}