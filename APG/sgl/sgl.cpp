//check-1 blya
//---------------------------------------------------------------------------
// sgl.cpp
// Empty implementation of the SGL (Simple Graphics Library)
// Date:  2016/10/24
// Author: Jaroslav Sloup
// Author: Jaroslav Krivanek, Jiri Bittner CTU Prague
// Edited: Jakub Hendrich, CTU Prague
//---------------------------------------------------------------------------

#include "sgl.h"
#include "ContextContainer.h"

/// Current error code.
static sglEErrorCode _libStatus = SGL_NO_ERROR;

ContextContainer* contextContainer = new ContextContainer(); // global
int MAX_SIZE = 40; // max number of contexts

static inline void setErrCode(sglEErrorCode c)
{
	if (_libStatus == SGL_NO_ERROR)
		_libStatus = c;
}

//---------------------------------------------------------------------------
// sglGetError()
//---------------------------------------------------------------------------
sglEErrorCode sglGetError(void)
{
	sglEErrorCode ret = _libStatus;
	_libStatus = SGL_NO_ERROR;
	return ret;
}

//---------------------------------------------------------------------------
// sglGetErrorString()
//---------------------------------------------------------------------------
const char* sglGetErrorString(sglEErrorCode error)
{
	static const char *errStrigTable[] =
	{
		"Operation succeeded",
		"Invalid argument(s) to a call",
		"Invalid enumeration argument(s) to a call",
		"Invalid call",
		"Quota of internal resources exceeded",
		"Internal library error",
		"Matrix stack overflow",
		"Matrix stack underflow",
		"Insufficient memory to finish the requested operation"
	};

	if ((int)error < (int)SGL_NO_ERROR || (int)error >(int)SGL_OUT_OF_MEMORY)
		return "Invalid value passed to sglGetErrorString()";

	return errStrigTable[(int)error];
}

//---------------------------------------------------------------------------
// Initialization functions
//---------------------------------------------------------------------------

void sglInit(void) {}

void sglFinish(void) {}

int sglCreateContext(int width, int height) { 
	if (contextContainer->getSize() > MAX_SIZE) { // max size 40
		setErrCode(SGL_OUT_OF_RESOURCES);
		return -1;
	}
	Context* context = new Context(width, height);
	if (context == 0) { // if pointer is null
		setErrCode(SGL_OUT_OF_MEMORY);
		return -1;
	}
	return contextContainer->add(context);
}

void sglDestroyContext(int id) {
	if (id >= contextContainer->getSize()) {
		setErrCode(SGL_INVALID_VALUE);
		return;
	}
	if (id == contextContainer->getContextId()) {
		setErrCode(SGL_INVALID_OPERATION);
		return;
	}
	contextContainer->remove(id);
}

void sglSetContext(int id) {
	if (id >= contextContainer->getSize()) {
		setErrCode(SGL_INVALID_VALUE);
		return;
	}
	contextContainer->setCurrent(id);
}

int sglGetContext(void) { 
	if (contextContainer->isEmpty()) {
		setErrCode(SGL_INVALID_OPERATION);
		return -1;
	}
	return contextContainer->getContextId(); 
}

float *sglGetColorBufferPointer(void) { 
	return contextContainer->getCurrent()->getColorBufferPointer(); 
}

//---------------------------------------------------------------------------
// Drawing functions
//---------------------------------------------------------------------------

void sglClearColor(float r, float g, float b, float alpha) {
	contextContainer->getCurrent()->setClearColor(Color(r, g, b));
}

void sglClear(unsigned what) { // WHAT IS what????
	// TODO - SGL_INVALID_VALUE Any bit other than those defined is set in the bitmask.
	if (contextContainer->isEmpty()) { // TODO sglClear() is called between a call to sglBegin() and the corresponding call to sglEnd().
		setErrCode(SGL_INVALID_OPERATION);
		return;
	}
	if (what & SGL_COLOR_BUFFER_BIT) {
		contextContainer->getCurrent()->clearColorBuffer();
	}
if (what & SGL_DEPTH_BUFFER_BIT) {
	contextContainer->getCurrent()->clearDepthBuffer();
}
}

void sglBegin(sglEElementType mode) {
	if (mode >= SGL_LAST_ELEMENT_TYPE) {
		setErrCode(SGL_INVALID_ENUM);
		return;
	}

	Context* context = contextContainer->getCurrent();

	if (context->isBusy()) {
		setErrCode(SGL_INVALID_OPERATION);
		return;
	}

	context->setBusy(true);

	context->setDrawingMode(mode);
}

void sglEnd(void) {
	Context* context = contextContainer->getCurrent();

	if (!context->isBusy()) {
		setErrCode(SGL_INVALID_OPERATION);
		return;
	}

	context->draw();
	context->clearVertexBuffer();
	context->clearVectorBuffer();
	context->setBusy(false);
}

void sglVertex4f(float x, float y, float z, float w) {
	contextContainer->getCurrent()->addVertex(Vertex(x, y, z, w));
}

void sglVertex3f(float x, float y, float z) {
	Context* context = contextContainer->getCurrent();

	if (context->isBeingDefined())
		context->addVector(Vector_3(x, y, z));
	else {
		context->addVertex(Vertex(x, y, z));
	}
}

void sglVertex2f(float x, float y) {
	Context* context = contextContainer->getCurrent();
	context->addVertex(Vertex(x, y));
}

void sglCircle(float x, float y, float z, float radius) {
	Context* context = contextContainer->getCurrent();

	context->addVertex(Vertex(x, y));
	Vertex center = context->vertexBuffer.at(context->vertexBuffer.size() - 1);
	context->vertexBuffer.pop_back();

	float sc = context->getScale();

	context->drawCircle(center, sc*radius);
}

void sglEllipse(float x, float y, float z, float a, float b) {
	Context* context = contextContainer->getCurrent();

	context->drawEllipse(Vertex(x, y), a, b);
}

void sglArc(float x, float y, float z, float radius, float from, float to) {
	Context* context = contextContainer->getCurrent();
	context->drawArc(Vertex(x, y), radius, from, to);
}

//---------------------------------------------------------------------------
// Transform functions
//---------------------------------------------------------------------------

void sglMatrixMode(sglEMatrixMode mode) {
	if (mode > SGL_PROJECTION) {
		setErrCode(SGL_INVALID_ENUM);
		return;
	}
	Context* context = contextContainer->getCurrent();

	if (context->isBusy()) {
		setErrCode(SGL_INVALID_OPERATION);
		return;
	}

	context->setMatrixMode(mode);
	context->setMatrix(mode, Matrix_4x4().createIdentityMatrix());
}

void sglPushMatrix(void) {
	Context* context = contextContainer->getCurrent();

	list<Matrix_4x4> stack = context->transformationStack;

	/*if (stack.max_size() == stack.size()) {
		setErrCode(SGL_STACK_OVERFLOW);
		return;
	}*/

	Matrix_4x4 top = context->getTopTransform();
	context->transformationStack.push_back(top);
}

void sglPopMatrix(void) {
	Context* context = contextContainer->getCurrent();

	list<Matrix_4x4> stack = context->transformationStack;


	if (stack.size() <= 0) {
		setErrCode(SGL_STACK_UNDERFLOW);
		return;
	}

	context->transformationStack.pop_back();
}

void sglLoadIdentity(void) {
	Context* context = contextContainer->getCurrent();

	if (context->isBusy()) {
		setErrCode(SGL_INVALID_OPERATION);
		return;
	}

	if (context->transformationStack.size() > 0) {
		context->transformationStack.back() = Matrix_4x4().createIdentityMatrix();
	}
	else {
		context->transformationStack.push_back(Matrix_4x4().createIdentityMatrix());
	}
}

void sglLoadMatrix(const float *matrix) {}

void sglMultMatrix(const float *matrix) {
	Context* context = contextContainer->getCurrent();

	Matrix_4x4 newMTransposed = Matrix_4x4().createTransposedMatrix(matrix);

	context->matrixMultiplication(newMTransposed);
}

void sglTranslate(float x, float y, float z) {

	Context* context = contextContainer->getCurrent(); 
	Matrix_4x4 top = context->getTopTransform();
	Matrix_4x4 newTop = top.multiply(Matrix_4x4().createTranslateMatrix(x, y, z));
	context->transformationStack.back() = newTop;
}

void sglScale(float scalex, float scaley, float scalez) {

	Context* context = contextContainer->getCurrent();

	Matrix_4x4 newTop = context->getTopTransform().multiply(Matrix_4x4().createScaleMatrix(scalex, scaley, scalez));
	context->transformationStack.back() = newTop;
}

void sglRotate2D(float angle, float centerx, float centery) {

	Context* context = contextContainer->getCurrent();
	Matrix_4x4 oldTop = context->getTopTransform();
	Matrix_4x4 newTop = oldTop.multiply(Matrix_4x4().createRotate2DMatrix(angle, centerx, centery));
	context->transformationStack.back() = newTop;
}

void sglRotateY(float angle) {
	Context* context = contextContainer->getCurrent();

	Matrix_4x4 newTop = context->getTopTransform().multiply(Matrix_4x4().createRotateYMatrix(angle));

	context->transformationStack.back() = newTop;
}

void sglOrtho(float left, float right, float bottom, float top, float near, float far) {
	if (left == right || top == bottom || near == far) {
		setErrCode(SGL_INVALID_VALUE);
		return;
	}

	Context* context = contextContainer->getCurrent();

	if (context->isBusy()) {
		setErrCode(SGL_INVALID_OPERATION);
		return;
	}

	context->orthoMultiplication(left, right, bottom, top, near, far);
}

void sglFrustum(float left, float right, float bottom, float top, float near, float far) {
	if (near <= 0.0f || far <= 0.0f) {
		setErrCode(SGL_INVALID_VALUE);
		return;
	}

	Context* context = contextContainer->getCurrent();

	if (context->isBusy()) {
		setErrCode(SGL_INVALID_OPERATION);
		return;
	}



	context->frustrumMultiplication(left, right, bottom, top, near, far);
}

void sglViewport(int x, int y, int width, int height) {
	if (width <= 0 || height <= 0) {
		setErrCode(SGL_INVALID_VALUE);
		return;
	}

	Context* context = contextContainer->getCurrent();

	if (context->isBusy()) {
		setErrCode(SGL_INVALID_OPERATION);
		return;
	}
	
	context->setViewportMatrix(x, y, width, height);
}

//---------------------------------------------------------------------------
// Attribute functions
//---------------------------------------------------------------------------

void sglColor3f(float r, float g, float b) {
	Context* context = contextContainer->getCurrent();

	if (context->isBusy()) {
		setErrCode(SGL_INVALID_OPERATION);
		return;
	}

	context->setCurrentColor(Color(r, g, b));
}

void sglAreaMode(sglEAreaMode mode) {
	if (mode > SGL_FILL) {
		setErrCode(SGL_INVALID_ENUM);
		return;
	}
	Context* context = contextContainer->getCurrent();

	if (context->isBusy()) {
		setErrCode(SGL_INVALID_OPERATION);
		return;
	}

	context->setAreaMode(mode);
}

void sglPointSize(float size) {
	Context* context = contextContainer->getCurrent();

	if (context->isBusy()) {
		setErrCode(SGL_INVALID_OPERATION);
		return;
	}

	context->setPointSize(size);
}

void sglEnable(sglEEnableFlags cap) {
	if (cap > SGL_DEPTH_TEST) {
		setErrCode(SGL_INVALID_ENUM);
		return;
	}
	Context* context = contextContainer->getCurrent();

	if (context->isBusy()) {
		setErrCode(SGL_INVALID_OPERATION);
		return;
	}
	if(cap == SGL_DEPTH_TEST){
		context->setDepthTest(true);
	}
}

void sglDisable(sglEEnableFlags cap) {
	if (cap > SGL_DEPTH_TEST) {
		setErrCode(SGL_INVALID_ENUM);
		return;
	}

	Context* context = contextContainer->getCurrent();

	if (context->isBusy()) {
		setErrCode(SGL_INVALID_OPERATION);
		return;
	}

	if (cap == SGL_DEPTH_TEST) {
		context->setDepthTest(false);
	}
	/*while(!context->transformationStack.empty())
	context->transformationStack.pop_back();*/
}

//---------------------------------------------------------------------------
// RayTracing oriented functions
//---------------------------------------------------------------------------

void sglBeginScene() {
	Context* context = contextContainer->getCurrent();
	context->setSceneDefinition(true);
}

void sglEndScene() {
	Context* context = contextContainer->getCurrent();
	context->setSceneDefinition(false);
}

void sglSphere(const float x,
	const float y,
	const float z,
	const float radius) {
	
	Context* context = contextContainer->getCurrent();

	if (!context->isBeingDefined())
	{
		setErrCode(SGL_INVALID_OPERATION);
		return;
	}

	context->addSphere(x, y, z, radius);
}

void sglMaterial(const float r,
	const float g,
	const float b,
	const float kd,
	const float ks,
	const float shine,
	const float T,
	const float ior) {
	
	Context* context = contextContainer->getCurrent();

	context->setCurrentMaterial(r, g, b, kd, ks, shine, T, ior);
}

void sglPointLight(const float x,
	const float y,
	const float z,
	const float r,
	const float g,
	const float b){
	
	Context* context = contextContainer->getCurrent();
	
	if (!context->isBeingDefined())	{
		setErrCode(SGL_INVALID_OPERATION);
		return;
	}
	
	PointLight light(Color(r, g, b), Vector_3(x, y, z));
	context->addPointLight(&light);	
}

void sglRayTraceScene() {
	Context* context = contextContainer->getCurrent();

	context->renderRayTracerScene();
}

// unused probably
void sglRasterizeScene() {}

void sglEnvironmentMap(const int width,
	const int height,
	float *texels) {

	Context* context = contextContainer->getCurrent();

	context->setEnvironmentMap(width, height, texels);
}

void sglEmissiveMaterial(const float r,
	const float g,
	const float b,
	const float c0,
	const float c1,
	const float c2){
	
	Context* context = contextContainer->getCurrent();

	if (!context->isBeingDefined())
	{
		setErrCode(SGL_INVALID_OPERATION);
		return;
	}

	context->setCurrentEmissiveMaterial(r, g, b, c0, c1, c2);
}