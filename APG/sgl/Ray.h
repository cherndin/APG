#pragma once
#ifndef RAY_H
#define RAY_H

#include "Vector_3.h"
#include "UsefulValues.h"

class Ray {
public:
	Ray();
	Ray(Vector_3 origin, Vector_3 direction);

	Ray(Vector_3 origin, Vector_3 direction, int depth);

	Ray(Vector_3 origin, Vector_3 direction, int tmin, int tmax);

	Ray(Vector_3 origin, Vector_3 direction, int depth, int tmin, int tmax);


	~Ray() {};


	Vector_3 origin;
	Vector_3 direction;

	/**
	* Calculates if a ray is refracted, sets rays direction
	* @param[in] ray ray which is the current ray casted from
	* @param[in] normal normal of a primitive
	* @param[in] ior index of refraction
	* @return boolean is ray refracted
	*/
	bool isRefracted(Ray ray, Vector_3 normal, float ior);

	void coutR() {
		origin.coutV();
		direction.coutV();		
	};

	int depth;
	
	float tmin, tmax;

};

#endif