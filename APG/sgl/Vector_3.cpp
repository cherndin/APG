#include "Vector_3.h"

Vector_3::Vector_3() {
	x = 0.0f;
	y = 0.0f;
	z = 0.0f;
}
//
//Vector_3::Vector_3(Vertex v) {
//	x = v.getX();
//	y = v.getY();
//	z = v.getZ();
//}

Vector_3::Vector_3(float x, float y, float z) : x(x), y(y), z(z) {

}
void Vector_3::setX(float value) { x = value; }
void Vector_3::setY(float value) { y = value; }
void Vector_3::setZ(float value) { z = value; }


float Vector_3::getX() { return x; }
float Vector_3::getY() { return y; }
float Vector_3::getZ() { return z; }


Vector_3 Vector_3::normalize() {
	float size = length();

	if (size == 0 || size == 1)
		return *this;

	x /= size;
	y /= size;
	z /= size;

	return *this;
}

Vector_3 Vector_3::subtract(Vector_3 v) {
	return Vector_3(x - v.getX(), y - v.getY(), z - v.getZ());
}

Vector_3 Vector_3::divide(float number) {
	return Vector_3(x/number, y/number, z/number);

}

Vector_3 Vector_3::add(Vector_3 v) {
	return Vector_3(x + v.getX(), y + v.getY(), z + v.getZ());
}


Vector_3 Vector_3::multiplyByFloat(float number) {
	return Vector_3(x * number, y * number, z * number);
}

Vector_3 Vector_3::crossProduct(Vector_3 v) {
	Vector_3 pr = Vector_3(y*v.getZ() - z*v.getY(), z*v.getX() - x*v.getZ(), x*v.getY() - y*v.getX());
	return pr;
}

float Vector_3::dotProduct(Vector_3 v) {
	float num = x * v.getX() + y * v.getY() + z * v.getZ();
	return num;
}

float Vector_3::length() {
	return sqrt(x * x + y * y + z * z);
}