#pragma once

#ifndef PRIMITIVE_H
#define PRIMITIVE_H

#include "Ray.h"

#include "Vertex.h"
#include "Vector_3.h"
#include "Material.h"
#include <cmath>
#include <iostream>



using namespace std;

class Primitive {
	public:
		Primitive() {};
		virtual ~Primitive() {};

		/**
		* Calculates if ray intersects with a primitive and if yes writes the distance to tHit
		* @param[in] ray
		* @param[in] tHit
		* @return boolean intersects
		*/
		bool intersect(Ray ray, float &tHit) { 
			return false; 
		};

		/**
		* Sets material to a primitive
		* @param[in] m material
		*/
		void setMaterial(Material m);

		/**
		* Gets material of a primitive
		* @return material
		*/
		Material getMaterial();

		/**
		* Sets emissive material to a primitive
		* @param[in] em material
		*/
		void setEmissiveMaterial(EmissiveMaterial em);

		/**
		* Gets normal of a primitive
		* @return normal
		*/
		Vector_3 getNormal();

		/**
		* Sets normal of a primitive
		* @param[in] v normal
		*/
		void setNormal(Vector_3 v);

		/**
		* Gets emissive material of a primitive
		* @return material
		*/
		EmissiveMaterial getEmissiveMaterial();

		/**
		* Is a primitive an area light
		*/
		bool areaLight = false;


private:
	Material material;
	EmissiveMaterial emissiveMaterial;
	Vector_3 normal;

};

class Triangle : public Primitive
{
	public:

		Triangle() {};

		Triangle(Vector_3 v1, Vector_3 v2, Vector_3 v3);

		bool intersect(Ray ray, float &tHit);

		void coutTr();

		float getArea();

		Vector_3 getRandomPoint();

		Vector_3 v1, v2, v3, ab, bc;




};

class Sphere : public Primitive
{
	public:
		Sphere(Vector_3 center, const float radius);

		bool intersect(Ray ray, float &tHit);

		/**
		* Gets normal of a primitive
		* @param[in] tHit distance to where ray intersects with a normal
		* @param[in] ray
		* @return normal
		*/
		Vector_3 getNormalOfSphere(float tHit, Ray ray);
		
		Vector_3 center;

		float radius;


};

#endif