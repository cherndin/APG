#include "ContextContainer.h"


int ContextContainer::add(Context* context) {
	container.push_back(context);
	return container.size()-1; // returns id
}

void ContextContainer::remove(int64_t id) {
	delete container[id]; // delete object from memory
	container[id] = 0; // null the pointer value
}


void ContextContainer::setCurrent(int64_t id) {
	current = container[id];
}

Context* ContextContainer::getCurrent() {
	return current;
}

int64_t ContextContainer::getContextId() {
	return container.size() - 1;
}

int64_t ContextContainer::getSize() {
	return container.size();
}

bool ContextContainer::isEmpty() {
	return container.size() == 0;
}

