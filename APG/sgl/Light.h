#pragma once
#ifndef LIGHT_H
#define LIGHT_H

#include <vector>

#include "Vector_3.h"
#include "Color.h"
#include "Primitive.h"
#include "Material.h"


#include <random>
#include <algorithm>
#include <math.h>

using namespace std;

class Light{};


class PointLight : public Light {

public:

	PointLight(Color color, Vector_3 position) : color(color), position(position) {};
	
	~PointLight() {};


	/**
	* Calculates color using phong lighting model
	* @param[in] material material of a primitive
	* @param[in] position position of an intersection of ray with a primitive
	* @param[in] camPos camera position
	* @param[in] normal normal of a primitive
	* @return color
	*/
	Color calculatePhongLight(Material material, Vector_3 position, Vector_3 camPos, Vector_3 normal);

	/**
	* Sets position
	* @param[in] position
	*/
	void setPosition(Vector_3 position);

	/**
	* Gets position
	* @return position
	*/
	Vector_3 getPosition();

	/**
	* Gets color
	* @return color
	*/
	Color getColor();



private:
	Color color;
	Vector_3 position;
};



class AreaLight {

public:

	AreaLight() {};

	AreaLight(EmissiveMaterial material);

	void addTriangle(Triangle* t);

	float getWeirdThing(float d);

	vector<Triangle*> triangles;

	EmissiveMaterial material;


};


#endif
