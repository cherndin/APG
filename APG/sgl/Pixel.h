#pragma once
#ifndef PIXEL_H
#define PIXEL_H

#include "stdint.h"

class Pixel {
public:
	Pixel(int64_t x, int64_t y, float z = 0) : x(x), y(y), z(z) {};

	Pixel() {};

	void setX(int64_t value);
	void setY(int64_t value);
	void setZ(float value);


	int64_t getX();
	int64_t getY();
	float getZ();


	~Pixel() {};

private:
	int64_t x, y;
	float z;

};

#endif