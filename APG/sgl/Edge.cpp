#include "Edge.h"

Edge::Edge(Vertex v1, Vertex v2) :v1(v1), v2(v2) {
	if (v1.getY() > v2.getY()) {
		swap(v1, v2);
	}
	x1 = v1.getX();
	y1 = v1.getY();

	x2 = v2.getX();
	y2 = v2.getY();

	zBottom = v1.getZ();
	zTop = v2.getZ();

}

bool Edge::equals(Edge e) {
	return v1.equals(e.v1) && v2.equals(e.v2);
}

void Edge::calculateIntersection(float y) {
	intersection = ((float)y*(x2 - x1) ) / (y2 - y1) + ((y2*x1 - y1*x2) / (y2 - y1)); 
}

void Edge::coutEdge() {
	cout << "v1: (" << x1 << ", " << y1 << "), v2: (" << x2 << ", " << y2 << ")" << endl;
}
