#ifndef VERTEX_H
#define VERTEX_H

#include "Matrix_4x4.h"

class Vertex {
public:

	Vertex();
	Vertex(float x, float y);
	Vertex(float x, float y, float z);
	Vertex(float x, float y, float z, float w);

	~Vertex() {};

	void setX(float value);
	void setY(float value);
	void setZ(float value);
	void setW(float value);

	bool equals(Vertex v);


	float getX();
	float getY();
	float getZ();
	float getW();

	Vertex& normalize();

	Vertex& multiply(Matrix_4x4& matrix);




private:
	float x, y, z, w;
};

#endif