#pragma once

#ifndef ENVIRONMENT_MAP_H
#define ENVIRONMENT_MAP_H

#include "Color.h"

class EnvironmentMap {
public:

	EnvironmentMap() {};
	EnvironmentMap(int width,
		int height,
		float *texels):width(width), height(height), texels(texels) {};

	~EnvironmentMap() {};

	float getWidth();

	float getHeight();

	Color getColor(float u, float v);
	
private:
	float width, height;
	float* texels;
};

#endif