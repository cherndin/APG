﻿#include "Context.h"

Context::Context(uint64_t width, uint64_t height) : width(width), height(height), windowSize(width*height), busy(false) {
	colorBuffer = new Color[width*height];
	currentColor = Color(1, 1, 1);
	pointSize = 3;
	areaMode = SGL_LINE;
	depthBuffer = new float[windowSize];
	clearDepthBuffer();
	areaLightsMode = false;
}

void Context::setColorOnPixel(Color color, Pixel pixel) {
	uint64_t index = pixel.getY()*width + pixel.getX();
	float z = pixel.getZ();
	if (index >= windowSize) {
		return;
	}	
	
	if (depthTest) {
		if (z < depthBuffer[index]) {
			depthBuffer[index] = z;
			colorBuffer[index] = color;
		}
		
	}
	else {
		colorBuffer[index] = color;

	}
}

float* Context::getColorBufferPointer() {
	return (float*)colorBuffer;
}


void Context::setClearColor(Color color) {
	clearColor = color;
	rayTracer.setBackgroundColor(color);
}

Color Context::getClearColor() {
	return clearColor;
}

void Context::setCurrentColor(Color color) {
	currentColor = color;
}

Color Context::getCurrentColor() {
	return currentColor;
}

void Context::setDepthTest(bool value) {
	depthTest = value;
}

bool Context::getDepthTest() {
	return depthTest;
}


void Context::clearColorBuffer() {
	for (uint64_t i = 0; i < windowSize; i++) {
		colorBuffer[i] = clearColor;
	}
}

bool Context::isBusy() {
	return busy;
}

void Context::setBusy(bool value) {
	busy = value;
}

void Context::setDrawingMode(sglEElementType mode) {
	drawingMode = mode;
}

sglEElementType Context::getDrawingMode() {
	return drawingMode;
}

void Context::setPointSize(float value) {
	pointSize = value;
}

float Context::getPointSize() {
	return pointSize;
}

int32_t Context::getTransformationStackSize() {
	return transformationStack.size();
}

void Context::addIdentityToTransformationStack() {
	transformationStack.push_back(Matrix_4x4().createIdentityMatrix());
}


void Context::addVertex(Vertex vertex) {

	Matrix_4x4 MVP = projectionMatrix.multiply(modelViewMatrix);

	vertex = vertex.multiply(transformationStack.back());
	
	vertex = vertex.multiply(MVP);

	vertex = vertex.normalize();

	vertex = vertex.multiply(viewportMatrix);

	vertexBuffer.push_back(vertex);
}

void Context::clearVertexBuffer() {
	vertexBuffer.clear();
}

void Context::setPixel(float x, float y, float z) {
	int64_t newX = (int64_t)x;
	int64_t newY = (int64_t)y;

	setColorOnPixel(currentColor, Pixel(newX, newY, z));
}

void Context::addVector(Vector_3 v) {
	vectorBuffer.push_back(v);
}

void Context::clearVectorBuffer() {
	vectorBuffer.clear();
}

void Context::draw() {
	if (defining) {
		if (drawingMode == SGL_POLYGON)	{
			if (vectorBuffer.size() == 3) {		
				addTriangle();
			}
		}
	} 
	else {
		if (areaMode == SGL_LINE) {
			if (drawingMode == SGL_POINTS) {
				rasterizePoints();
			}
			else if (drawingMode == SGL_LINES || drawingMode == SGL_LINE_STRIP || drawingMode == SGL_LINE_LOOP) {
				rasterizeLines(drawingMode);
			}
			else if (drawingMode == SGL_POLYGON) {
				drawingMode = SGL_LINE_LOOP;
				rasterizeLines(drawingMode);
			}
		}
		else if (areaMode == SGL_FILL) {
			if (drawingMode == SGL_POLYGON) {
				rasterizeFilledPolygon();
			}
		}
	}

}

void Context::rasterizePoints() {
	float radius = (pointSize) / 2.0;

	for (vector<Vertex>::iterator it = vertexBuffer.begin(); it != vertexBuffer.end(); it++) {
		Vertex vertex = *it;
		float x = vertex.getX();
		float y = vertex.getY();

		// x and y axis start at the left top corner

		// first find all corners of the square
		float leftUpX = x - radius;
		float leftUpY = y + radius; 
		float leftDownY = y - radius;
	
		float pointHeight = fabs(leftDownY - leftUpY); 


		// fills all pixels in between
		float kX = round(leftUpX);
		float kY = round(leftDownY);

		for (int64_t i = kY; i < kY + pointHeight; i++) {
			for (int64_t j = kX; j < kX + pointHeight; j++) {
				int64_t kx = j;
				int64_t ky = i;
				setPixel(kx, ky);
			}
		}
	}
}
//Zdroj: https://rosettacode.org/wiki/Bitmap/Bresenham%27s_line_algorithm
void Context::rasterizeLine(Vertex v1, Vertex v2) {

	int64_t x1 = (int64_t)v1.getX();
	int64_t y1 = (int64_t)v1.getY();

	int64_t x2 = (int64_t)v2.getX();
	int64_t y2 = (int64_t)v2.getY();
//	cout << "v1: (" << x1 << ", " << y1 << "), v2: (" << x2 << ", " << y2 << ")" << endl;

	if (y1 == y2) {
		fillBetweenTwoPoints(x1, x2, y1);
	}

	
	bool steep = (fabs(y2 - y1) > fabs(x2 - x1));
	if (steep)
	{
		swap(x1, y1);
		swap(x2, y2);
	}

	if (x1 > x2)
	{
		swap(x1, x2);
		swap(y1, y2);
	}

	float dx = x2 - x1;
	float dy = fabs(y2 - y1);

	float error = dx / 2.0f;
	int ystep = (y1 < y2) ? 1 : -1;

	int y = y1;
	for (int x = x1; x < x2; x++)
	{
		steep ? setPixel(y, x) : setPixel(x, y);

		error -= dy;
		if (error < 0)
		{
			y += ystep;
			error += dx;
		}
	}
	setPixel(v2.getX(), v2.getY());
}


void Context::rasterizeLines(sglEElementType mode) {
	if (mode == SGL_LINES) {
			Vertex v1, v2;
			for (vector<Vertex>::iterator it = vertexBuffer.begin(); it != vertexBuffer.end(); it += 2) {
				v1 = *it;
				v2 = *(it + 1);
				rasterizeLine(v1, v2);

			}
	}
	else if (mode == SGL_LINE_STRIP) {
		Vertex v1, v2;

		for (vector<Vertex>::iterator it = vertexBuffer.begin(); it != vertexBuffer.end(); ++it) {
			v2 = *it;
			if (it == vertexBuffer.begin()) {
				v1 = v2;
				continue;
			}
			rasterizeLine(v1, v2);
			v1 = v2;
		}
	}
	else if (mode == SGL_LINE_LOOP) {
		Vertex v1, v2;

		for (vector<Vertex>::iterator it = vertexBuffer.begin(); it != vertexBuffer.end(); ++it) {
			v2 = *it;
			if (it == vertexBuffer.begin()) {
				v1 = v2;
				continue;
			}
			rasterizeLine(v1, v2);
			v1 = v2;
		}
		if (vertexBuffer.size() > 0)
			rasterizeLine(v1, vertexBuffer.at(0));
	}
}

void Context::setMatrixMode(sglEMatrixMode mode) {
	matrixMode = mode;
}

sglEMatrixMode Context::getMatrixMode() {
	return matrixMode;
}

void Context::setViewportMatrix(uint64_t x, uint64_t y, uint64_t width, uint64_t height) {
	viewportMatrix = Matrix_4x4().createViewportMatrix(x, y, width, height);
}

Matrix_4x4 Context::getViewportMatrix() {
	return viewportMatrix;
}

void Context::setMatrix(sglEMatrixMode mode, Matrix_4x4 matrix) {
	if (mode == SGL_PROJECTION) {
		projectionMatrix = matrix;
		//projectionMatrix.coutMatrix();
	}
	else if (mode == SGL_MODELVIEW) {
		modelViewMatrix = matrix;
	}
}

Matrix_4x4 Context::getMatrix(sglEMatrixMode mode) {
	if (mode == SGL_PROJECTION) {
		return projectionMatrix;
	}
	else if (mode == SGL_MODELVIEW) {
		return modelViewMatrix;
	}
	return modelViewMatrix;
}

void Context::orthoMultiplication(float left, float right, float bottom, float top, float near, float far) {
	Matrix_4x4 ortho = Matrix_4x4().createOrthoMatrix(left, right, bottom, top, near, far);

	Matrix_4x4 mult = getMatrix(matrixMode).multiply(ortho);
	
	setMatrix(matrixMode, mult);
}

void Context::matrixMultiplication(Matrix_4x4 m) {
	Matrix_4x4 mult = getMatrix(matrixMode).multiply(m);

	setMatrix(matrixMode, mult);
}

void Context::frustrumMultiplication(float left, float right, float bottom, float top, float near, float far) {

	Matrix_4x4 frustrum = Matrix_4x4().createFrustrumMatrix(left, right, bottom, top, near, far);

	Matrix_4x4 mult = getMatrix(matrixMode).multiply(frustrum);

	setMatrix(matrixMode, mult);
}

void Context::drawCircle(Vertex v, float r) {
	if (areaMode == SGL_LINE) {
		drawCircle(v, r, false);
	} else if (areaMode == SGL_FILL) {
		drawCircle(v, r, true);
	}
}


void Context::drawCircle(Vertex v, float r, bool fill) {
	if (fill) {
		//fillBetweenTwoPoints(v.getX() - r, v.getX() + r, v.getY());
	}
	int x, y, p;
	int xs = v.getX();
	int ys = v.getY();
	x = 0;
	y = r;
	p = 3 - 2 * r;
	while (x <= y) {
		if (fill) {
			fillBetweenTwoPoints(xs - x, xs + x, ys + y);
			fillBetweenTwoPoints(xs - y, xs + y, ys + x);
			fillBetweenTwoPoints(xs - y, xs + y, ys - x);
			fillBetweenTwoPoints(xs - x, xs + x, ys - y);
		}
		else {
			setSymPixels(x, y, xs, ys);
		}
		if (p < 0)
			p = p + 4 * x + 6;
		else {
			p = p + 4 * (x - y) + 10;
			y = y - 1;
		}
		x = x + 1;
	}
	//if (x == y) setSymPixels(x, y, xs, ys);


}



void Context::setSymPixels(int x, int y, int xc, int yc)
{
	setPixel(x + xc, y + yc);
	setPixel(-x + xc, y + yc);
	setPixel(x + xc, -y + yc);
	setPixel(-x + xc, -y + yc);
	setPixel(y + xc, x + yc);
	setPixel(y + xc, -x + yc);
	setPixel(-y + xc, x + yc);
	setPixel(-y + xc, -x + yc);
}

void Context::drawEllipse(Vertex v, float a, float b, bool fill)
{
	float x,y;
	float centerX = v.getX();
	float centerY = v.getY();


	int ellipseSegments = 40;
	float alpha = (2*M_PI) / ellipseSegments;

	for (int i = 0; i < ellipseSegments; ++i)
	{
		x = a * sin(i * alpha);
		y = b * cos(i * alpha);
		addVertex(Vertex(x + centerX, y + centerY));
	}
	if (fill) {
		//fillBetweenTwoPoints(centerX, centerX + 1000, centerY);
		rasterizeFilledPolygon();
	}
	else {
		drawingMode = SGL_LINE_LOOP;
		draw();
	}
	clearVertexBuffer();
}

void Context::drawEllipse(Vertex v, float a, float b) {
	if (areaMode == SGL_LINE) {
		drawEllipse(v, a, b, false);
	}
	else if (areaMode == SGL_FILL) {
		drawEllipse(v, a, b, true);
	}
}

void Context::drawArc(Vertex v, float radius, float from, float to, bool fill)
{
	float x, y;
	if (from > to) swap(from, to);

	float dia = (to - from);

	int elipseSegments = ceil((40 * dia) / (2.0 * M_PI));
	float alpha = dia/elipseSegments;

	if (fill) {
		addVertex(Vertex(v.getX(), v.getY()));
	}
	for (int i = 0; i <= elipseSegments; ++i)
	{
		float angle = i * alpha;
		x = radius * cos(angle + from);
		y = radius * sin(angle + from);
		addVertex(Vertex(x + v.getX(), y + v.getY()));
	}
	if (fill) {
		addVertex(Vertex(v.getX(), v.getY()));
		rasterizeFilledPolygon();
	}
	else {
		drawingMode = SGL_LINE_STRIP;
		draw();
	}
	clearVertexBuffer();
}

void Context::drawArc(Vertex v, float radius, float from, float to) {
	if (areaMode == SGL_LINE) {
		drawArc(v, radius, from, to, false);
	}
	else if (areaMode == SGL_FILL) {
		drawArc(v, radius, from, to, true);

	}
}

	

float Context::getScale() {
	float det = (((float(width) / 2.0) * projectionMatrix.matrix[0] * modelViewMatrix.matrix[0] *
		(float(height) / 2.0) * projectionMatrix.matrix[5] * modelViewMatrix.matrix[5]) -
		(float(width) / 2.0) * projectionMatrix.matrix[1] * modelViewMatrix.matrix[1] *
			(float(height) / 2.0) * projectionMatrix.matrix[4] * modelViewMatrix.matrix[4]);
	float sqr = sqrt(det);
	return sqr;
}

Matrix_4x4 Context::getTopTransform() {
	if (transformationStack.size() <= 0) {
		Matrix_4x4 MVP = projectionMatrix.multiply(modelViewMatrix);
		transformationStack.push_back(MVP);
	}
		
		
	return transformationStack.back();
}

void Context::setAreaMode(sglEAreaMode mode) {
	areaMode = mode;
}

void Context::rasterizeFilledPolygon() {
	float maxY = FLOAT_MIN_VALUE;
	float minY = FLOAT_MAX_VALUE;

	vector<float> maxis;
	for (vector<Vertex>::iterator it = vertexBuffer.begin(); it != vertexBuffer.end();)	{
		Vertex tmp = *it;

		if (++it == vertexBuffer.end())
			break;

		Edge e(tmp, *it);

		edges.push_back(e);	

		maxY = max(maxY, e.v2.getY());
		maxY = max(maxY, e.v1.getY());

		minY = min(minY, e.v1.getY());
		minY = min(minY, e.v2.getY());
	}

	// add the last and first vertices edge
	Edge e1(vertexBuffer.back(), vertexBuffer.front());
	edges.push_back(e1);
	//e1.coutEdge();
	
	vector<Edge> activeEdges;

	for (float i = maxY; i > minY; i--) {
		//cout << "Y: " << i << endl;
		for (vector<Edge>::iterator edgeIterator = edges.begin(); edgeIterator != edges.end(); edgeIterator++) {

			Edge e2 = *edgeIterator;
			//e2.coutEdge();
				
			if (e2.y1 == e2.y2) {
				continue;
			}
			if ((i < e2.y2 && i < e2.y1) || (i > e2.y2 && i > e2.y1)) {
				continue;
			}

			e2.calculateIntersection(i);
			activeEdges.push_back(e2);		
		}


		if (activeEdges.size() % 2 == 1) {
			activeEdges.pop_back();
		}

		if (activeEdges.empty()) {
			continue;
		}
		
		// SORT ACTIVE EDGES BY INTERSECTION
		sort(activeEdges.begin(), activeEdges.end(), Context::sortEdges());

		vector<Edge> specialEdges; // for lines that start off from the begginning&end of 2 edges and end up in the begginning&end of another 2 edges
		for (vector<Edge>::iterator it = activeEdges.begin(); it != activeEdges.end(); it += 2) {
			Edge edge1 = (*it);
			Edge edge2 = (*(it+1));

			float from = edge1.intersection;
			float to = edge2.intersection;	

			if (edge1.y1 == edge2.y2 || edge1.y2 == edge2.y1) {
				specialEdges.push_back(edge1);
				continue;
			}
			// cout << "FROM : " << from << ", TO : " << to << endl;]
			
			if (depthTest) {
				fill(edge1.y1, edge1.y2, edge2.y1, edge2.y2, edge1.zBottom, edge1.zTop, edge2.zBottom, edge2.zTop, from, to, (float)i);
			}
			else {
				fillBetweenTwoPoints(from, to, i);
			}
		}		

		activeEdges.clear();
		

		if (specialEdges.size() % 2 == 1) {
			specialEdges.pop_back();
		}

		if (specialEdges.empty()) {
			continue;
		}

		for (vector<Edge>::iterator it = specialEdges.begin(); it != specialEdges.end(); it += 2) {
			Edge edge1 = (*it);
			Edge edge2 = (*(it + 1));

			float from = edge1.intersection;
			float to = edge2.intersection;
			if (depthTest) {
				fill(edge1.y1, edge1.y2, edge2.y1, edge2.y2, edge1.zBottom, edge1.zTop, edge2.zBottom, edge2.zTop, from, to, i);
			}
			else {
				fillBetweenTwoPoints(from, to, i);
			}
		}


		specialEdges.clear();

	}
	edges.clear();

	
}

void Context::fill(float yBottom1, float yTop1, float yBottom2, float yTop2, float zBottom1, float zTop1, float zBottom2, float zTop2, float from, float to, float y) {

	float alpha1 = (float)(fabs(y - yTop1)) / fabs(yBottom1 - yTop1);
	float alpha2 = (float)(fabs(y - yTop2)) / fabs(yBottom2 - yTop2);

	float z1 = (1.0 - alpha1) * zTop1 + alpha1 * zBottom1; // z of first line segment
	float z2 = (1.0 - alpha2) * zTop2 + alpha2 * zBottom2; // z of second line segment
	//cout << "Z1 : " << z1 << ", Z2 : " << z2 << endl;

	fillBetweenTwoPoints(from, to, y, z1, z2);

}



void Context::fillBetweenTwoPoints(float x1, float x2, float y){
	int64_t from = ceil(x1);
	int64_t to = ceil(x2);
	int64_t g = ceil(y);

	/*if (g == 286 || g == 314) {
		int h = 0;
	}*/
	//gecka.push_back(g);

	if (from == to) {
		setPixel(from, g);
		return;
	}
	if (from > to)
		std::swap(from, to);

	for (int64_t x = from; x < to; ++x) {
		setPixel(x, g, 0);
	}
}

void Context::fillBetweenTwoPoints(float x1, float x2, int64_t y, float d_a, float d_b) {
	int64_t from = (int64_t)x1;
	int64_t to = (int64_t)x2;

	if (from > to)
		std::swap(from, to);


	for (int64_t x = from; x <= to; ++x) {
		float alpha = fabs(x - x1) / fabs(x2 - x1);
		float zInterpolated = (1 - alpha) * d_a + alpha * d_b;
		//cout << "Z : " << zInterpolated<< endl;

		setPixel(x, y, zInterpolated);
	}
}

void Context::clearDepthBuffer() {
	for (uint64_t i = 0; i < windowSize; ++i) {
		depthBuffer[i] = FLOAT_MAX_VALUE;

	}
}


bool Context::isBeingDefined() {
	return defining;
}

void Context::setSceneDefinition(bool statement) {
	defining = statement;
}

void Context::addSphere(float x, float y, float z, float radius) {
	Primitive* sphere = new Sphere(Vector_3(x, y, z), radius);
	sphere->setMaterial(material);
	rayTracer.addPrimitive(sphere);
}

void Context::setEnvironmentMap(int width, int height, float *texels) {
	rayTracer.setEnvironmentMap(new EnvironmentMap(width, height, texels));
}


void Context::renderRayTracerScene() {

	Matrix_4x4 MVP = projectionMatrix.multiply(modelViewMatrix);

	Matrix_4x4 MVPV = viewportMatrix.multiply(MVP);
	Matrix_4x4 MVPVinverse = (viewportMatrix.multiply(MVP)).inverse();
	Matrix_4x4 MVinverse = modelViewMatrix.inverse();
	Matrix_4x4 MVPinverse = MVP.inverse();

	Vector_4 camViewport(0.0, 0.0, 0.0, 1.0);
	Vector_4 camModel = MVinverse.multiplyByVector_4(camViewport);
	Vector_3 from = camModel.divideByW();




	for (uint32_t y = 0; y < height; ++y) {
		for (uint32_t x = 0; x < width; ++x) {

			//if (x == 285 && y == 128) {
			//	int g = 0;
			//	//setColorOnPixel(Color(1,0,0), Pixel(x, y));
			//}

			/*if (x == 128 && y == 128) {
				int g = 0;
				setColorOnPixel(Color(1,0,0), Pixel(x, y));
			}*/

			Vector_4 pixelViewport((float)x, (float)y, -1.0, 1.0);
			Vector_4 pixelModel = MVPVinverse.multiplyByVector_4(pixelViewport);
			Vector_3 to = pixelModel.divideByW();
			Ray ray(from, (to.subtract(from)).normalize());
			Color color = rayTracer.castRay(ray);

			setColorOnPixel(color, Pixel(x, y));
		}
	}

	
	
	rayTracer.clearLights();
}



void Context::addTriangle() {
	if (areaLightsMode) {
		Triangle *t = new Triangle(vectorBuffer[0],	vectorBuffer[1], vectorBuffer[2]);

		areaLight->addTriangle(t);
		material = Material(emissiveMaterial.color);

		// add area light to primitives
		t->areaLight = true;
		t->setMaterial(material);
		rayTracer.addPrimitive(t);
	}
	else {
		Triangle *t = new Triangle(vectorBuffer[0], vectorBuffer[1], vectorBuffer[2]);


		t->setMaterial(material);

		//t->coutTr();

		rayTracer.addPrimitive(t);
	}
	
}


void Context::addPointLight(PointLight* p) {
	//p->getPosition().coutV();
	rayTracer.addPointLight(p);
}

void Context::setCurrentMaterial(const float r,
	const float g,
	const float b,
	const float kd,
	const float ks,
	const float shine,
	const float T,
	const float ior) {

	material = Material(r, g, b, kd, ks, shine, T, ior);
}

void Context::setCurrentEmissiveMaterial(const float r,
	const float g,
	const float b,
	const float c0,
	const float c1,
	const float c2) {

	emissiveMaterial = EmissiveMaterial(r, g, b, c0, c1, c2);

	areaLight = new AreaLight(emissiveMaterial);

	rayTracer.addAreaLight(areaLight);

	areaLightsMode = true;
}