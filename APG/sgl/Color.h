#ifndef COLOR_H
#define COLOR_H

#include <iostream>

using namespace std;
class Color {
public:

	Color() {};
	Color(float r, float g, float b);
	~Color() {};

	void setR(float value);
	void setG(float value);
	void setB(float value);

	float getR();
	float gerG();
	float getB();

	void coutCol();

	Color add(Color c);

	Color multiply(Color c);

	Color multiply(float c);


private:
	float r = 0.0f;
	float g = 0.0f;
	float b = 0.0f;
};

#endif