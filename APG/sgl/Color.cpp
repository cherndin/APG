#include "Color.h"


Color::Color(float r, float g, float b):r(r), g(g), b(b) {

}

void Color::setR(float value) { r = value; }
void Color::setG(float value) { g = value; }
void Color::setB(float value) { b = value; }

float Color::getR() { return r; }
float Color::gerG() { return g; }
float Color::getB() { return b; }

Color Color::add(Color c) {
	r += c.r;
	g += c.g;
	b += c.b;
	return *this;

}

void Color::coutCol() {
	cout << r << " " << g << " " << b << endl;
}

Color Color::multiply(Color c) {
	r *= c.r;
	g *= c.g;
	b *= c.b;
	return *this;
}

Color Color::multiply(float c) {
	r *= c;
	g *= c;
	b *= c;
	return *this;

}
