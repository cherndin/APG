#include "Pixel.h"


void Pixel::setX(int64_t value) { x = value; }
void Pixel::setY(int64_t value) { y = value; }
void Pixel::setZ(float value) { z = value; }


int64_t Pixel::getX() { return x; }
int64_t Pixel::getY() { return y; }
float Pixel::getZ() { return z; }