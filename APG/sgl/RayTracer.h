#pragma once

#ifndef RAYTRACER_H
#define RAYTRACER_H

#include <vector>


#include "Vertex.h"
#include "Light.h"
#include "Primitive.h"
#include "Color.h"
#include "Ray.h"
#include "Vector_3.h"
#include "Vector_4.h"
#include "EnvironmentMap.h"
#include "Primitive.h"



using namespace std;


class RayTracer {
	public:


		RayTracer() {
			envMap = nullptr;
		};

		~RayTracer() {};
		
		/**
		* Sets MVP matrix
		* @param[in] m MVP matrix
		*/
		void setMVP(Matrix_4x4 m);

		/**
		* Sets viewport matrix
		* @param[in] m viewport matrix
		*/
		void setViewport(Matrix_4x4 m);


		/**
		* Casts ray into the scene and calculates phong lighting model, casts further reflected and refracted rays
		* @param[in] ray ray casted
		* @return color calculated with phong
		*/
		Color castRay(Ray ray);


		/**
		* Adds primitive to a list
		* @param[in] p primitive(triangle or sphere)
		*/
		void addPrimitive(Primitive* p);

		
		/**
		* Adds point light to a list
		* @param[in] light point light
		*/
		void addPointLight(PointLight* light);


		/**
		* Adds area light to a list
		* @param[in] light area light
		*/
		void addAreaLight(AreaLight* light);
		
		/**
		* Sets background color
		* @param[in] color background color
		*/
		void setBackgroundColor(Color color);

		/**
		* ets background color
		* @return background color
		*/
		Color getBackgroundColor();


		/**
		* Sets environment map
		* @param[in] map environment map
		*/
		void setEnvironmentMap(EnvironmentMap* map);

	
		/**
		* Casts ray into the scene and calculates phong lighting model, casts further reflected and refracted rays
		* @param[in] rayFromLightToPrim ray from a light source to a primitive
		* @param[in] distanceToOriginalPrim distance to an original primitive that ray has intersected
		* @return boolean is ray in shadow
		*/
		bool isInShadow(Ray rayFromLightToPrim, float distanceToOriginalPrim);

		
		/**
		* Casts ray into the scene and calculates phong lighting model, casts further reflected and refracted rays
		* @param[in] rayFromLightToPrim ray from a light source to a primitive
		* @param[in] distanceToOriginalPrim distance to an original primitive that ray has intersected
		* @return boolean is ray in shadow
		*/
		bool intersectRayWithPrimitive(Ray ray, Primitive* p, float &tHit);

		/**
		* Clears lights
		*/
		void clearLights();


		/**
		* Returns environment color, if none env map is used, returns background color
		* @param[in] ray
		* @return environment color
		*/
		Color getEnvironmentColor(Ray ray);


		/**
		* Send ray further if the material of a primitive is either reflective or refractive
		* @param[in] ray
		* @param[in] hit primitive 
		* @param[in] normal normal of a primitive
		* @param[in] intersection point of intersection of a ray with the primitive
		* @return ray tracing result color
		*/
		Color sendRayFurther(Ray ray, Primitive* hit, Vector_3 normal, Vector_3 intersection);



		Matrix_4x4 MVP;
		Matrix_4x4 MVPinverse;
		Matrix_4x4 viewport;
		Matrix_4x4 viewportInverse;

		// primitive list
		vector<Primitive*> primitives;

		// point light list
		vector<PointLight> pointLights;

		// area light list
		vector<AreaLight*> areaLights;

		// background color
		Color backgroundColor;

		// environment map
		EnvironmentMap* envMap;

		// depth oof ray tracer
		int DEPTH = 8;

		// shift for the ray when is cast to the next level
		const float shift = 0.01;

		// number of samples from area light
		const int NUM_SAMPLES_AREA_LIGHT = 16;
};

#endif