﻿#include "Primitive.h"


void Primitive::setMaterial(Material m) {
	material = m;
}

void Primitive::setEmissiveMaterial(EmissiveMaterial em) {
	emissiveMaterial = em;
}

void Primitive::setNormal(Vector_3 v) {
	normal = v;
}

Triangle::Triangle(Vector_3 v1, Vector_3 v2, Vector_3 v3) :v1(v1), v2(v2), v3(v3) {
	ab = v2.subtract(v1);
	bc = v3.subtract(v1);
	setNormal(ab.crossProduct(bc).normalize());
	areaLight = false;
}

float Triangle::getArea() {
	return 0.5 * ab.crossProduct(bc).length();
}

Vector_3 Triangle::getRandomPoint() {
	float random1 = (float)(rand()) / RAND_MAX;
	float random2 = (float)(rand()) / RAND_MAX;
	float a = 1.0 - sqrt(random1);
	float b = sqrt(random1) * (1.0 - random2);

	Vector_3 randomPoint = v1.add(ab.multiplyByFloat(a)).add(bc.multiplyByFloat(b));
	return randomPoint;
}

//bool Triangle::intersect(Ray ray, float &tHit) {
//	Vector_3 s1 = ray.direction.crossProduct(bc);
//	float divisor = s1.dotProduct(ab);
//	if (divisor == 0)
//		return false;
//	float invDivisor = 1.f / divisor;
//
//	int i = (int)invDivisor;
//	// Compute first barycentric coordinate
//	Vector_3 d = ray.origin.subtract(v1); //v1.subtract(ray.origin);// 
//	float b1 = d.dotProduct(s1) * invDivisor;
//	if (b1 < 0. || b1 > 1.)
//		return false;
//	// Compute second barycentric coordinate
//	Vector_3 s2 = d.crossProduct(ab);
//	float b2 = ray.direction.dotProduct(s2) * invDivisor;
//	if (b2 < 0. || b1 + b2 > 1.)
//		return false;
//	// Compute _t_ to intersection point
//	float t = bc.dotProduct(s2) * invDivisor;
//	if (t < ray.tmin || t > ray.tmax)
//		return false;
//	tHit = t;
//	return true;
//}
//


// Zdroj: semestralka Vadim Petrov a Johana Emma Křečková
bool Triangle::intersect(Ray ray, float &tHit) 	{
	Vector_3 n = getNormal();
	if (ray.direction.dotProduct(n) > 0.0f) return false;

	Vector_3 u, v;  // triangle vectors
	Vector_3 w0, w; // ray vectors
	Vector_3 I;     // intersection
	float dot1, dot2;  // params to calc ray-plane intersect

	// get triangle edge vectors and plane normal
	u = ab;
	v = bc;

	w0 = ray.origin.subtract(v1);
	dot1 = -n.dotProduct(w0);
	dot2 = n.dotProduct(ray.direction);
	if (fabs(dot2) < 0.01f) return false; // ray is  parallel to triangle plane

	// get intersect point of ray with triangle plane
	tHit = dot1 / dot2;
	if (tHit < 0.0f) return false; // ray goes away from triangle

	I = ray.origin.add(ray.direction.multiplyByFloat(tHit)) ; // intersect point of ray and plane

										// is I inside triangle?
	float uu, uv, vv, wu, wv, D;
	uu = u.dotProduct(u);// dot(u, u);
	uv = u.dotProduct(v); //dot(u, v);
	vv = v.dotProduct(v);
	w = I.subtract(v1);
	wu = w.dotProduct(u);//dot(w, u);
	wv = w.dotProduct(v); // dot(w, v);
	D = uv * uv - uu * vv;
	
	// get and test parametric coords
	float s, t;
	s = (uv * wv - vv * wu) / D;
	if (s < 0.0 || s > 1.0) return false; // I is outside T
	t = (uv * wu - uu * wv) / D;
	if (t < 0.0 || (s + t) > 1.0) return false; // I is outside T

	return true;

}

Sphere::Sphere(Vector_3 center, const float radius) :center(center), radius(radius) { 

	areaLight = false;
}

bool Sphere::intersect(Ray ray, float &tHit){
	Vector_3 toEye = ray.origin.subtract(center);
	float b = toEye.dotProduct(ray.direction);
	float c = toEye.dotProduct(toEye) - radius * radius;
	float d = b * b - c;

	if (d > 0.0f) {
		float t = -b - sqrtf(d);
		if (t < 0.0f) t = -b + sqrtf(d);
		tHit = t;
		return true;
	}
	return false;
}

Vector_3 Primitive::getNormal() {
	return normal;
}

Material Primitive::getMaterial() {
	return material;
}
Vector_3 Sphere::getNormalOfSphere(float tHit, Ray ray) {
	Vector_3 norm = ray.origin.add(ray.direction.multiplyByFloat(tHit)).subtract(center).multiplyByFloat(radius).normalize();
	return norm;
}

EmissiveMaterial Primitive::getEmissiveMaterial() {
	return emissiveMaterial;
}

void Triangle::coutTr() {
	cout << "TRIANGLE : " << endl;
	v1.coutV();
	v2.coutV();
	v3.coutV();
}
