#pragma once

#ifndef DEFINES_H
#define DEFINES_H

#include <limits>
#include <algorithm>

#define _USE_MATH_DEFINES

using namespace std;



const float FLOAT_MAX_VALUE = numeric_limits<float>::max();

const float FLOAT_MIN_VALUE = numeric_limits<float>::min();

const int64_t INT_MIN_VALUE = numeric_limits<int64_t>::min();

const int64_t INT_MAX_VALUE = numeric_limits<int64_t>::max();




#endif