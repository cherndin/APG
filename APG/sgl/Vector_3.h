#pragma once
#ifndef VECTOR_3_H
#define VECTOR_3_H

#include <limits>
#include <iostream>
#include <cmath>

//#include "Vertex.h"
//#include "Matrix_4x4.h"

using namespace std;
class Vector_3 {
public:

	Vector_3();
	Vector_3(float x, float y, float z);
//	Vector_3(Vertex v);


	void setX(float value);
	void setY(float value);
	void setZ(float value);

	float getX();
	float getY();
	float getZ();

	Vector_3 normalize(); // Zdroj : http://www.fundza.com/vectors/normalize/

	Vector_3 subtract(Vector_3 v);

	Vector_3 divide(float number);

	Vector_3 add(Vector_3 v);

	Vector_3 multiplyByFloat(float number);

	Vector_3 crossProduct(Vector_3 v);

	float dotProduct(Vector_3 v);

	float length();

	void coutV() {
		cout << x << " " << y << " " << z << endl;
	};



private:
	float x, y, z;
};

#endif