#include "EnvironmentMap.h"


float EnvironmentMap::getWidth() {
	return width;
}
float EnvironmentMap::getHeight() {
	return height;
}


Color EnvironmentMap::getColor(float u, float v) {
	int x = u * width;
	int y = (1.0 - v) * height;
	int pos = (x + y * width) * 3;
	Color c = Color(texels[pos], texels[pos + 1], texels[pos + 2]);
	return c;
}