#include "Vertex.h"

Vertex::Vertex() {
	x = 0.0f;
	y = 0.0f;
	w = 1.0f;
	z = 0.0f;
}

Vertex::Vertex(float x, float y, float z) : x(x), y(y), z(z) { 
	w = 1.0f;
}

Vertex::Vertex(float x, float y) : x(x), y(y) {
	w = 1.0f;
	z = 0.0f;
}

Vertex::Vertex(float x, float y, float z, float w) : x(x), y(y), z(z), w(w) { }

bool Vertex::equals(Vertex v) {
	return x == v.getX() && y == v.getY() && z == v.getZ() && w == v.getW();
}

void Vertex::setX(float value) { x = value; }
void Vertex::setY(float value) { y = value; }
void Vertex::setZ(float value) { z = value; }
void Vertex::setW(float value) { w = value; }


float Vertex::getX() { return x; }
float Vertex::getY() { return y; }
float Vertex::getZ() { return z; }
float Vertex::getW() { return w; }


Vertex& Vertex::normalize()
{
	if (w == 0.0f)
		return *this;

	x /= w;
	y /= w;
	z /= w;

	w = 1.0f;

	return *this;
}

Vertex& Vertex::multiply(Matrix_4x4& matrix) {
	float x1 = x;
	float y1 = y;
	float z1 = z;
	float w1 = w;

	x = x1 * matrix.matrix[0] + y1 * matrix.matrix[1] + z1 * matrix.matrix[2] + w1 * matrix.matrix[3];
	y = x1 * matrix.matrix[4] + y1 * matrix.matrix[5] + z1 * matrix.matrix[6] + w1 * matrix.matrix[7];
	z = x1 * matrix.matrix[8] + y1 * matrix.matrix[9] + z1 * matrix.matrix[10] + w1 * matrix.matrix[11];
	w = x1 * matrix.matrix[12] + y1 * matrix.matrix[13] + z1 * matrix.matrix[14] + w1 * matrix.matrix[15];

	return *this;
}


